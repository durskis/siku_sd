/*!

  \file forces_mass.cc

  \brief Implementation of forces_mass function

*/

#include "forces_mass.hh"
#include "errors.hh"
#include "coordinates.hh"
#include <math.h>

using namespace Geometry;
using namespace Coordinates;

#include <iostream> // for some tests

// ----------------------------- local utils --------------------------------

inline void _drag_factors( Globals& siku, Element& e,
                           double& water_factor, double& wind_factor )
{
  // yet simple scaling by constants from python. May me changed to
  // multiparametric algorithm later.
  water_factor = e.anchority * siku.phys_consts["anchority"];
  wind_factor  = e.windage   * siku.phys_consts["windage"];
}



inline void _turning_angles( Globals& siku, Element& e,
                           double& water_angle, double& wind_angle )
{
  // yet simple scaling by constants from python. May me changed to
  // multiparametric algorithm later.
  water_angle = e.water_angle * siku.phys_consts["water_angle"];
  wind_angle  = e.wind_angle * siku.phys_consts["wind_angle"];
}

inline void _rot_freq( Globals& siku, Element& e,
                           double& TwoOmega )
{
  // yet simple scaling by constants from python. May me changed to
  // multiparametric algorithm later.
  TwoOmega = 2.0 * 2.0 * M_PI * e.periodRot / siku.phys_consts["periodRot"];
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void forces_mass( Globals& siku )
{
  for ( size_t i = 0; i < siku.es.size (); ++i )
    {
      //error-safety??
      //if( siku.es[i].flag & Element::F_ERRORED ) continue;

      if(  siku.es[i].flag & Element::F_STEADY    // coz steady and static
        || siku.es[i].flag & Element::F_STATIC
        || siku.es[i].flag & Element::F_STATIC_SLIP
        || siku.es[i].flag & Element::F_ANAVEL
        || siku.es[i].flag & Element::F_CONTROLLED
        || siku.es[i].flag & Element::F_UNDRAGGED ) //CSD no drag applied
        continue;

      // i[Gleb?] just don`t like range-based loops
      auto & e = siku.es[i];

      // force scaling
      double wnd_fact, wat_fact;
      _drag_factors( siku, e, wat_fact, wnd_fact );
      // force turning
      double wat_ang, wnd_ang;
      _turning_angles(siku, e, wat_ang, wnd_ang );


      // calculating element`s speed in local coords
      vec3d V = e.V;
      //if(!_verify(abs(V)))  cout<<"-----"<<V<<endl;

      //-------- WIND ----------

      // acquiring element` position in terms lat-lon
      double lat, lon;
      Coordinates::sph_by_quat ( e.q, &lat, &lon );
      
      // interpolating wind speed near element`s mass center
      vec3d W = siku.wind.get_at_lat_lon_rad ( Coordinates::norm_lat( lat ),
                                               Coordinates::norm_lon( lon ) );

      // CSD TEMPORARILY zero the winds.
      /*W.x=0.0;
      W.y=0.0;
      W.z=0.0; */

      // transforming to local coordinates
      W = Coordinates::glob_to_loc( e.q, W );

      //CSD Include a wind rotation angle.  (Rotate forcing to the left
      //CSD relative to the resultant (wind-element) vel-vector. ...
      //CSD kinda inefficient to redo this over and over but ...

      mat3d Wrot(cos(wnd_ang/180*M_PI), sin(wnd_ang/180*M_PI),0,
                -sin(wnd_ang/180*M_PI), cos(wnd_ang/180*M_PI),0,0,0,0);
      W = W * Wrot;

      // velocity difference between ice element and wind
      W -= V;

      // calculating local Force (draft)
      e.F += W * abs( W ) * e.A * siku.planet.R2 * wnd_fact ;

      //-------- WATER (yet steady) ----------

      // interpolating currents speed
      // !!check for earth.R scaling
      W = siku.water.get_at_lat_lon_rad ( Coordinates::norm_lat( lat ),
                                          Coordinates::norm_lon( lon ) );

      // transforming currents into local coords
      W = Coordinates::glob_to_loc( e.q, W );

      //CSD Include a water rotation angle.  (Rotate forcing to the left
      //CSD relative to the resulatant (water-element) vel vector.
      mat3d Wrotw (cos(wat_ang/180*M_PI), -sin(wat_ang/180*M_PI),0,
                  sin(wat_ang/180*M_PI), cos(wat_ang/180*M_PI),0,0,0,0);
      W = W * Wrotw;

      // velocity difference between ice element and water
      W -= V;

      // applying water forces
      e.F += W * abs( W ) * e.A * siku.planet.R2 * wat_fact;

      // rotation slow down
      e.N -= wat_fact * (e.i * siku.planet.R2)
              * (e.A * siku.planet.R2) * e.W.z;

      // improve: add torque caused by Curls of water and wind fields.

    }
  for ( size_t i = 0; i < siku.es.size (); ++i )
    {
      //CSD Now let's try to add the coriolis force.
      // CSD the funny thing to remember here is that our V field
      // does not have our typical geophysical orientation.
      // v is the first (x) component of V and is positive eastward.
      // u is positive southward. It is -V.y.
      // so the CorRot matrix defined here needs to fit with this 'novel'
      // choice of coordinates.
      auto & e = siku.es[i];
      // acquiring element` position in terms lat-lon
      double lat, lon;
      Coordinates::sph_by_quat ( e.q, &lat, &lon );

      if(  siku.es[i].flag & Element::F_STEADY    // coz steady and static
        || siku.es[i].flag & Element::F_STATIC
        || siku.es[i].flag & Element::F_STATIC_SLIP
        || siku.es[i].flag & Element::F_ANAVEL
        || siku.es[i].flag & Element::F_CONTROLLED)  // won`t change their speed
        continue;
      double TwoOmega;
      _rot_freq( siku, e, TwoOmega);

      mat3d CorRot (            0.0, -sin(lat+0.7854), 0.0,    \
                    sin(lat+0.7854),              0.0, 0.0,    \
                                0.0,              0.0, 0.0);
      // e.F+= e.m * CorRot * V * TwoOmega;
       //CSD the wind drag and water drage terms in e.F have the dimensions
      //CSD of an actual force if the water_fact and wind_fac are density weighted.
      // CSD (that is they have units of density ). With this in mind, the
      // CSD coriolis term should be just as we'd expect it to be, as above (m*f*v)
      vec3d Cor_force = e.m * TwoOmega * CorRot * e.V;
      e.F += Cor_force;
    }
  // manual forces
  for( size_t i = 0; i < siku.man_inds.size(); ++i )
    {
      // indexes of manually added forces
      size_t I = siku.man_inds[i];
      vec3d tv;

      double lat, lon;
      sph_by_quat ( siku.es[I].q, &lat, &lon );

      tv = glob_to_loc ( siku.es[I].q, geo_to_cart_surf_velo(
          lat, lon, siku.man_forces[i].x, siku.man_forces[i].y ) );

      vec3d F = tv;
      double trq = siku.man_forces[i].z;

      // TODO: check planet.R scaling
      siku.es[I].flag |= Element::F_SPECIAL;
      siku.es[I].F += F * siku.planet.R;
      siku.es[I].N += trq;
    }

}
