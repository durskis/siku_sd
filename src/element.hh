/*
 * Siku: Discrete element method sea-ice model: element.hh
 *       Ice Element class: ice element representation and methods
 *
 * Copyright (C) UAF
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

/*! \file element.hh
 *  \brief class element for representing ice elements
 */

#ifndef ELEMENT_HH
#define ELEMENT_HH

#include <cstdint>
#include <vector>
#include <string>
using namespace std;

#include "siku.hh"
#include "coordinates.hh"

//! \brief Ice element class: representation for a single ice element
class Element
{
public:
  //! \brief struct for marking and saving vertices
  struct vertex
  {
    unsigned long elem_id;
    vec3d pos;
    size_t face_damage_c;
    size_t face_damage_t;
    size_t face_damage_s;

    vertex(const vec3d& v = {}, const size_t& id = 0,
           const size_t& d_c=0, const size_t& d_t=0,
           const size_t& d_s=0)
    //: pos( v ), elem_id( id ) {} // <- dis spawns a lot of warnings
    {
      pos = v;
      elem_id = id;
      face_damage_c = d_c;
      face_damage_t = d_t;
      face_damage_s = d_s;
    }
  };

  //! \brief flag state for free body element
  static const unsigned int F_FREE   {0x1};
  //! \brief flag state for steady body element, that remains same v
  //! and w as at the beginning
  static const unsigned int F_STEADY {0x2};
  //! \brief flag state for static body element with always v=0, w=0
  static const unsigned int F_STATIC {0x4};
  //! \brief flag state for controlled object that looks for control
  //! function to determine v and w
  static const unsigned int F_CONTROLLED {0x8};
  //! \brief flag state for all movement states (filtering all other flags)
  //CSD add a static element that does not bond to other elements around it
  static const unsigned int F_STATIC_SLIP {0x10};
  static const unsigned int F_SPECIAL {0x20}; // aka 32
  static const unsigned int F_UNBONDABLE {0x40};
  //! \brief Let's add a flag for an analytical velocity as a function of time.
  static const unsigned int F_ANAVEL {0x80};
  //! \brief flag for runtime land-fastened ice elements
  static const unsigned int F_FASTENED {0x100};  // aka 128
  //CSD let's allow for only select elements to experience wind or
  // water drag
  static const unsigned int F_UNDRAGGED {0x200};
  //! \brief technical flag for NOT_NEW elements, means those elements were
  //! either loaded from snapshot or already processed by dynamics/position
  //CSD this is for some sort of restart with added elements, have never used.
  static const unsigned int F_PROCESSED {0x400};  // aka 64

  //CSD following flag is antiquated since there are movement states
  //CSD beyond 0x7 .. may need to clean this up further.
  //! \brief flag state for all movement states (filtering all other flags)
  static const unsigned int F_MOVE_FLAG {0x7};

  //! \brief flag state for monitored object that exchanges
  //! information with python script
  //CSD MONITORED is currently unused.
  //| static const unsigned int F_MONITORED  {0x10};
  //! \brief special flag for special 'marked' elements



  //! \brief flag state for elements with any kind of error properties
  static const unsigned int F_ERRORED {0x80000000};


  // --------------- Not changing handling parameters ----------------

  unsigned int flag;                  //!< state flag
  size_t mon_ind { 0 };               //!< monitor function index
  size_t con_ind { 0 };               //!< control function index

  size_t id { 0 };                    //!< id of element, matches it`s index
                                      //! in Globals.es

  // --------------- Rapidly changing parameters ----------------------

  //! \brief "Pendulum" orientation: defines the position of the ice
  //! element on a sphere
  quat q;

  vec3d Glob;           //!< global position in (x, y, z)
  vec3d V{};            //!< local surface velocity (x, y, 0)

  double m;             //!< kg, mass (SI)
  double I;             //!< kg*m^2, moment of inertia (SI)

  vec3d W;              //!< 1/m, angular velocity in local coord.
  vec3d F;              //!< N, net force vector in local frame
  double N {0};         //!< N*m, torque value in local frame (SI)

  double OA {0};        //!< m^2, total overlap area with landfast ice
  double Amin {0};      //!< m^2, minimal area of polygons for fastening checks

  double Sxx {0};       //!< N/m^2, stress tensor 'xx' component
  double Syy {0};       //!< N/m^2, stress tensor 'yy' component
  double Sxy {0};       //!< N/m^2, stress tensor 'xy' component
  double Syx {0};       //!< N/m^2, stress tensor 'yx' component
  //CSD add principal stress estimate
  double S1  {0};
  double S2  {0};
  double shear_angle {0};

  vec2d Vnudge {1,1};        //CSD an element-based viscous parameter I'm tryong out for boundary nudging

  double h_main{0};     //!< m, thickness of the main (thickest) layer
  //?double elast{0};       //!< kg/s^2, elasticity of element (SI)
  vector <size_t> face_damage_c;  //CSD added damage accumulators at element level
  vector <size_t> face_damage_t;
  vector <size_t> face_damage_s;
  // --------------- Not changing state parameters -------------------

  size_t imat;                  //!< material index
  size_t igroup;                //!< group index
  double i;                     //!< I/m, geometical moment of inertia
  double A;                     //!< area of polygon (on unit sphere)
  double sbb_rmin;              //!< bounding sphere minimum radius

  double anchority;             //!< interaction with water
  double windage;               //!< interaction with wind
  double water_angle;           //!< ocean current turning angle (degrees)
  double wind_angle;            //;< wind turning angle (degrees)

  double periodRot;             //!< planetarary rotation
  double nudge_fac;             //CSD nudging factor for F_CONTROLLED b.c.
  double bnd_resist_fac;        //CSD resistance factor for F_CONTROLLED b.c.
  //OLD //vector<double> gh;
  double gh[ MAT_LAY_AMO ];     //!< g(h) thickness distribution
  vector<vec3d> P;              //!< 1, local unit frame coords of
                                //! vertices
  vector<vec3d> P_g;            //CSD global position of vertices
  // ------------------- METHODS: -------------------------------------

  // improve: use geometry:: function instead of this!
  //! Check if point (given inglobal x,y,z ) is inside the element
  bool contains( const vec3d& p );


  //! Destructor
  // Deprecated-> 'takes care about monitor and control functions'
  ~Element()
  {
  }
};

//====================================================================

//! \brief supporting class for file input/output
class PlainElement
{
public:
  // --------------- Not changing handling parameters ----------------

  unsigned int flag { 0 };
  unsigned long mon_ind { 0 };
  unsigned long con_ind { 0 };
  unsigned long id { 0 };

  // --------------- Rapidly changing parameters ----------------------

  quat q;
  vec3d Glob = nullvec3d;
  vec3d V = nullvec3d;
  double m { 0 };
  double I { 0 };
  vec3d W = nullvec3d;
  vec3d F = nullvec3d;
  double N {0};
// CSD add stress components to Plain Element for output
  double Sxx {0};       //!< N/m^2, stress tensor 'xx' component
  double Syy {0};       //!< N/m^2, stress tensor 'yy' component
  double Sxy {0};       //!< N/m^2, stress tensor 'xy' component
  double Syx {0};       //!< N/m^2, stress tensor 'yx' component
  double S1  {0};
  double S2  {0};
  double shear_angle {0};
  vector <size_t> face_damage_c;  //CSD added damage accumulators at element level
  vector <size_t> face_damage_t;
  vector <size_t> face_damage_s;

  // --------------- Not changing state parameters -------------------

  unsigned long imat { 0 };
  unsigned long igroup { 0 };
  double i { 0 };
  double A { 0 };
  double sbb_rmin { 0 };

  double gh[ MAT_LAY_AMO ];
};

#endif
