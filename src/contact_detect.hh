/*
 * Siku: Discrete element method sea-ice model
 *
 * Copyright (C) UAF
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#ifndef INTERACTION_HH
#define INTERACTION_HH

#include "siku.hh"
#include "element.hh"
#include "coordinates.hh"
#include <boost/circular_buffer.hpp>
#include <vector>

// predeclaration due to circled includes (yes, we have spaghetti-code)
struct Globals;

// contact detection methods classification
enum : unsigned long
{
  CONTACTS_N2 = 0,
  SWEEP_N_PRUNE = 1
};

// enum of methods for contact detection frequency calculation
enum : unsigned long
{
  ALWAYS = 0,
  BY_TICKS = 2,
  BY_SECONDS = 3,
  BY_SPEED = 4
};

// contact state flag (outside of classes for fast access)
enum ContType: unsigned long
{
  NONE = 0,
  COLLISION = 1,
  JOINT = 2,  // aka coalesced
  COMPRESSIVE_FAILURE = 3,  // CSD save history of what was once a joint that failed
                           // in compression and still exhibits overlapping elements
  SHEAR_CMP_FAILURE = 4,   //CSD failed in shear while under compression
  INIT_COLLISION = 5       // CSD track contacts that are initial collisions

};

enum FailType: unsigned long
{
  INTACT = 0,
  COMPRESSION = 1,
  TENSION = 2,  // aka coalesced
  SHEAR = 3  // CSD save history of what was once a joint that failed
};
//! \brief utility struct: pairs of indexes of initially connected polygons.
//! Is used for loading from python and initial freezing method.
struct Link
{
  unsigned long i1;
  unsigned long i2;
  Link() = default;
  Link( unsigned long _i1, unsigned long _i2 )
  {
    if( _i1 < _i2 )
      {
        i1 = _i1;
        i2 = _i2;
      }
    else
      {
        i1 = _i2;
        i2 = _i1;
      }
  }

  inline bool operator < ( const Link& L ) const
  {
    return i1 < L.i1 || ( i1 == L.i1 && i2 < L.i2 );
  }
};

//! \brief Functions class: provides several methods for contact detection,
//! storage and history access
class ContactDetector
{

public:
  //! \brief Inner structure for holding interaction pairs metadata
  struct Contact
  {
    ContType type { NONE };
    FailType failure_status { INTACT };
    size_t i1 { 0 };
    size_t i2 { 0 };

    int step{ -1 };  // step when was created. -1 marks default object
    double area{ 0. };  // area of contact (on unit sphere)
    double durability{ 1. };  // IMPROVE: must be discussed
    // TODO: discuss and change following names
    double init_len{ 0. };  // initial width (OUS) Must be discussed
    double init_wid{ 0. }; // initial length (OUS) Must be discussed
    double init_area{0.};  //CSD initial area of intersection of elements
                           //    initially in collision contact
    int generation{ 0 };  // 'oldness'
    double Ss{ 0. };
    double Sn{ 0. };

    double W_strain{ 0.};   //CSD energy associated with  elastic strain, or collision
    double W_couple{ 0.};   //CSD energy associated with bond rotational resistance
    double W_fric{ 0.};     //CSD energy associated with frictional loss

    double shr_weakening{ 1.0 };
    double nrm_weakening{ 1.0 };

    typedef boost::circular_buffer<vec2d> cb_vec2d;              // CSD store 6 previous values of va12 so that a
                                  // time-averaged value could be used in the frictional
                                  // estimate.
    cb_vec2d va12_cb{6};

    vec2d F_elj{};  //CSD track contact elastic force for joints, for post-joint failure adjustments
    // In different physics approaches the points` meaning is different:
    // in _test_spring p1 and p2 are the positions of 'joint point' in
    // local coords of 1st and 2nd polygons, in _dist_springs: p1, p2 -
    // positions in 1st poly, p3,p4 - 2nd (as the approach uses 2 springs)
    // Note...I was assuming the above, the calculation of p did not change
    // with contact physics. I think the dist_springs was just wrong.
    Geometry::vec2d p1{}, p2{},  // positions of joint centerS in local
                    p3{}, p4{};  // coords of contacting polygons

    vec2d r1, r2;   // CSD some diagnostic quantities
    double gamma{1e-4}; // scaling factor between collision and joint forcing
    double fric_dissp{0.0}; //sliding frictional dissipation coefficient. (collision)
    size_t v11 {};  // indexes of vertices of shearing edge
    size_t v12 {};
    size_t v21 {};
    size_t v22 {};

    //CSD save the neighboring element vertex position in local frames
    //CSD note: may not need the last two actually
    Geometry::vec3d t21_1{}, t21_2{}, t12_1{}, t12_2{};

    int face_1 {0};
    int face_2 {0};

    //! \brief Search for common (or hopefully the closest) edge of two
    // elements in contact.
    //! \return: mean length of edges on success, 0 on failure
    double find_edges( Globals& siku );
//    double find_edges_( Globals& siku );//OLD

    // -------------------------------

    Contact(){}
    Contact(const size_t& i1_, const size_t& i2_, const int& s,
            const ContType& ct = ContType::NONE): step(s)
    {
      // first element in contact should remain first in list (lower id)
      if( i1_ < i2_)
        {
          i1 = i1_;
          i2 = i2_;
        }
      else
        {
          i1 = i2_;
          i2 = i1_;
        }
    }

    inline bool operator < ( const Contact& c ) const
    {
      return i1 < c.i1 || (i1 == c.i1 && i2 < c.i2);
    }
    inline bool operator == ( const Contact& c ) const
    {
      return ( i1 == c.i1 ) && ( i2 == c.i2 );
    }
    inline bool operator != ( const Contact& c ) const
    {
      return ( i1 != c.i1 ) || ( i2 != c.i2 );
    }
  };

  //! \brief Method specifier (ye, i know what 'meth' means...)
  unsigned long det_meth{ CONTACTS_N2 };

  //! \brief detection frequency type
  unsigned long det_freq_t{ ALWAYS };

  //! \brief flag for inital freezing options
  unsigned long inital_freeze{ 0 };

  //! \brief contact detection XX value (XX may be period, speed, etc)
  double det_value;

  std::vector<Link> links;

private:
  //! \brief util value to store previous 'det_value'
  double det_last{ 0. };
public:

  // contacts pool
  std::vector < Contact > cont;

  //------------------------------- methods ----------------------------------

  //! \brief Method for calling contacts detection
  void detect( Globals& siku );

  //! \brief Method for generating 'joint' contacts - hard frozen connections.
  //! Second optional argument - tolerance of ice elements size. It is actually
  //! a scaling factor for temporal resizing for overlap detection.
  void freeze( Globals& siku, double tolerance = 0.0001 );

  //! \brief Method for generating initial connections of polygons based on
  //! hit data in scenario script
  void freeze_links( Globals& siku );

private:
  //! \brief method to check if it is time to update contacts
  bool is_detect_time( Globals& siku );

  //! \brief simple method for contacts detection. N^2 complexity.
  void find_pairs( Globals& siku );

  //! \brief sweep and prune method for contacts detection.
  void sweep_n_prune( Globals& siku );

  //! \brief smart cleaning of contacts list. 'Frozen' (coalesced) contacts
  //! remain untouched until destroyed, other are renewed at each step
  void clear();

};

#endif
