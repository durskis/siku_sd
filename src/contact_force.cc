/*
 * Siku: Discrete element method sea-ice model
 *
 * Copyright (C) UAF
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <cmath>

#include "contact_force.hh"
#include <boost/circular_buffer.hpp>

#include <fstream>

/*
 * TODO: discuss threading VS static
 */

//using namespace BG;
using namespace Coordinates;
using namespace Geometry;

// ======================== local utility structs ===========================

// local structure for intersection (overlapping) data.
// !members` declaration order supposed to be optimized by memory
struct ContactData
{
  vector<vec2d> loc_P1;         // e1.P vertices in local 2D coords
  vector<vec2d> loc_P2;         // e2.P vertices in local 2D coords
  vector<vec2d> loc_P1_2;         // e1.P vertices in local 2D coords
  vector<vec2d> loc_P2_2;         // e2.P vertices in local 2D coords
  vector<vec2d> interPoly;      // intersection polygon
  vector<vec2d> interPoly2;      // intersection polygon
  vector<PointStatus> stats, stats2;    // statuses of points in interPoly

  vector<size_t> edge_inds;          // interpoly vertices of polygon edge intersection points.
  vector<size_t> edge_inds2;         // interpoly vertices ... in e2 frame
  mat3d e1_to_e2;               // matrix for coordinates transformation
  mat3d e2_to_e1;               // between e1 and e2 local systems

  vec2d icen, icen2,                  // vec from e1 center to intersection center
        r1,                     // vec from e1 center to contact region center
        r2,                     // vec from e2 center to contact region center
        r12;                    // vec from e1 center to e2 center
                                // (on unit sphere)

                                // velocities of points located in the center
                                // of interPoly but belonging to e1 and e2:
  vec2d va1,                    //   e1 point relative to e1 (steady) center
        va2,                    //   e2 point relative to e2 (steady) center
        va12;                   //   e2 point relative to e1 point
                                // ( SI )
  vec2d va1r,                   //CSD rotational velocity of interpoly center in e1
        va2r;                   //    rotational velocity ... e2

  double area, area2;                  // area of interPoly - area of overlap
  double area_diff;             //CSD difference in area estimate in e1 and e2 frames
                                // (on unit sphere)
  double d1, d2;                // deformations of contact considering
                                // it to be quadrangle ( m, SI )
  vec2d dr1, dr2;               // CSD 2D strain vectors for dist.sprngs.

  vec2d ed1;                    // CSD 2D joint-edge orientation unit vector,
                                // orthog(ed1) points into the element.
  vec2d ed2;

  ContactDetector::Contact& c;  // some references
  Globals & siku;
  Element & e1, & e2;

  int inter_res;                // amount of intersection points (just in case)

  // ALL values are being calculated in constructor
  ContactData( ContactDetector::Contact& _c, Globals& _siku ):
    siku( _siku ), c( _c ), e1( siku.es[ c.i1 ] ), e2( siku.es[ c.i2 ] ),
    d1{}, d2{}
  {
//    VERIFY( e1.q, "CollDat");
//    VERIFY( e2.q, "CollDat");

    // coordinates transformation matrixes (local systems of two elements)
    e2_to_e1 = loc_to_loc_mat( e1.q, e2.q );
    e1_to_e2 = loc_to_loc_mat( e2.q, e1.q );

    // polygons in local (e1) coords
    for( auto& p : e1.P ) loc_P1.push_back( vec3_to_vec2( p ) );
    for( auto& p : e2.P ) loc_P2.push_back( vec3_to_vec2( e2_to_e1 * p ) );

    // errors check
//    if( errored( loc_P1 ) )   e1.flag |= Element::F_ERRORED;
//    if( errored( loc_P2 ) )   e2.flag |= Element::F_ERRORED;

    // call for 'geometry'->'2d'->'polygon intersection'
    // CSD it can happen that there is a roundoff error area of intersection
    // in the coordinates of one element, giving inter_res=3, but no
    // intersection in the coordinates of the other element.
    // In this case we will assume there is no intersection
    long inter_res1, inter_res2;
    inter_res1 = intersect( loc_P1, loc_P2, interPoly, &stats, &icen, &area );

    /* CSD let's repeat above to get intersection polygon in local (e2) frame
     as well  ...allthough it seems a bit convoluted */
    for( auto& p : e1.P ) loc_P1_2.push_back( vec3_to_vec2( e1_to_e2 * p ) );
    for( auto& p : e2.P ) loc_P2_2.push_back( vec3_to_vec2( p ) );
    inter_res2 = intersect( loc_P1_2, loc_P2_2, interPoly2, &stats2, &icen2, &area2 );

    area_diff = abs(area-area2);
    // some security for 1D and 0D intersections
    inter_res = min(inter_res1, inter_res2);
    if( inter_res < 3 ) area = 0.;

    // and calc centers` interpositions
    r12 = vec3_to_vec2( e2_to_e1 * NORTH );
    if( c.type == JOINT  || c.type == COMPRESSIVE_FAILURE
        || c.type == SHEAR_CMP_FAILURE)
      {
        // TODO: reorganize this properly
        //CSD I don't see a good reason to be averaging r1 and r2 here
        // if one element is bigger than the other the r's may be
        // significantly different lengths. The torque to apply to e1
        // should not be a function of the distance to the center of e2.
        // so I'm not too sure about this formulation
       r1 = (c.p1 + c.p2) * 0.5;
       r2 = vec3_to_vec2( e2_to_e1 * vec2_to_vec3( (c.p3 + c.p4) * 0.5 ) );


       r1 = (r1 + (r12 + r2)) * 0.5;
      }
     else
        r1 = icen;
        //CSD this is for r2 in e1 frame.
        r2 = r1 - r12;

    // CSD Let's determine the two 'EDGE' vertices of the intersection polygon
    // for calculation of the normal and the force point (Feng, 2003)
    // CSD this find is probably no faster than a for loop
     //CSD Rather, we need to determine all the interpoly edges to accurately represent
     // the force for more complex element intersections.
    // CSD This section of code is no longer useful. These calculations are
     // performed in _elastic_force_new. We were incorrectly redefining r1 and
     //   r2 below.

     if( inter_res >= 3 && c.type!=JOINT ) {
         /*std::vector<Geometry::PointStatus>::iterator found_edge1 = std::find(stats.begin(), stats.end(), Geometry::EDGE);
        edge_inds[0]= std::distance(stats.begin(), found_edge1);
       std::vector<Geometry::PointStatus>::iterator found_edge2 = std::find(++found_edge1, stats.end(), Geometry::EDGE);
        edge_inds[1]= std::distance(found_edge1, found_edge2)+std::distance(stats.begin(), found_edge1); */
         for (int i=0;i<stats.size();i++) {
             if (stats[i]==Geometry::EDGE){
                 edge_inds.push_back(i);
             }
         }
         /*if (edge_inds.size()>=2) {
             // CSD For collisions, specify the Force point as the midpoint of the
             // line between the edge points of the interpoly.
             vec2d p1p2[2];
             p1p2[0] = interPoly[edge_inds[0]];  // interpoly edge points
             p1p2[1] = interPoly[edge_inds[1]];
             //CSD let's explore a bit here what different r1's look like
             vec2d mid_point[edge_inds.size()];
             for (int i=0;i<edge_inds.size()-1;i++) {
                 mid_point[i] = 0.5 * (interPoly[edge_inds[i]] +
                                       interPoly[edge_inds[i+1]]);
             }
             mid_point[edge_inds.size()-1] = 0.5 * (interPoly[edge_inds[0]] +
                     interPoly[edge_inds[edge_inds.size()-1]]);
             //r1 = 0.5 * ( p1p2[0] + p1p2[1] );
             //r2 = r1 - r12;
         } */
     }
    // CSD the location of application of the force for energy conservation
    //  according to Feng et al 2003,should be at the midpoint of the bisector
    //  of the intersection polygon, or the midpoint of the overlapping segment
    //  of the collision face. So we need
    // IMPROVE: check order of planet.R carefully!
    // e1 aim speed (coz of propagation + spin)
    va1 = vec3_to_vec2_s( e1.V )
        + rot_90_cw( r1 ) * ( -e1.W.z ) * siku.planet.R;
    // e2 aim speed (propagation + spin)
    vec3d tv = lay_on_surf( e2_to_e1 * e2.V );
    va2 = vec3_to_vec2_s( lay_on_surf( e2_to_e1 * e2.V ) )
        + rot_90_cw( r2 ) * ( -e2.W.z ) * siku.planet.R;
    //CSD let's track the interpoly rotational velocity
    va1r = rot_90_cw( r1 ) * ( -e1.W.z ) * siku.planet.R;
    va2r = rot_90_cw( r2 ) * ( -e2.W.z ) * siku.planet.R;
    // velocity difference at aim point
    va12 = va1 - va2;
    vec2d va12t2 = va1;
    vec2d va12t3 = va2;
    vec2d va12t4 = va1-va2;
    vec2d zerov = {0. , 0.};
    // vec3d va12t3 = {va12.x, va12.y};

  }

};

struct InterForces
{
  // all vectors in `Element1` local coordinates ( SI )
  vec2d rf1,        // application point of force at e1
        rf2,        // application point of force at e2, (in e1 frame)
        F1,         // force applied to e1 ( = - F2 )
        F2;         // CSD force applied to e2, useful to distinguish for f_controlled
  double couple1,   // couple (wikipedia) applied to e1 ( = - couple2 ) ( SI )
         couple2;
};

// ==================== local functions` declarations =======================

InterForces _collision( ContactData& cd );

InterForces _test_springs( ContactData& cd );

InterForces _hopkins_frankenstein( ContactData& cd );

InterForces _distributed_springs( ContactData& cd );

InterForces _wilch_like( ContactData& cd );

InterForces _edge_orient( ContactData& cd );

// -----------------------------------------------------------------------

// calculates linear rigidity of ice with respect to material and other props
inline double _rigidity( ContactData& cd )
{
  // ice thickness at largest (main) layer
  double h1 = cd.e1.h_main, h2 = cd.e2.h_main;

  // elasticity of elements
  double E1 = cd.siku.ms[cd.e1.imat].E, E2 = cd.siku.ms[cd.e2.imat].E;
  //CSD have the accumulated damage to an element face reduce the rigidity
  //CS but only for imat = 0, assuming other materials are coasts, etc.
  // The way this is hacked currently, the non-ice elements have half the spring
  // constant if the E for the coast material (imat=1) is set the same as that for the ice (imat=0).
  //CSD What I'm doing here is not right. If I want the ice to weaken, I need to
  // allow the ice imat=0 material to weaken. (Changing this 11.14.22)
  double damage_1, damage_2;

  if (cd.e1.face_damage_c[cd.c.face_1] + cd.e1.face_damage_t[cd.c.face_1]>2)
  {
     double dtseconds=cd.siku.time.get_dt();
     //CSD we need to figure out a more rigorous way to specify a damage rate that
     // is consistent with the rate of change of durability, or eliminate durability
     // all together.
     double damage_rate = 0.1;   //CSD 0.1 ~ 10 seconds to break a bond.
     /* double damg = (cd.e1.face_damage_c[cd.c.face_1] + cd.e1.face_damage_t[cd.c.face_1]-2)
                   * dtseconds * damage_rate; */
     double damg = max(double(cd.e1.face_damage_c[cd.c.face_1]
                            + cd.e1.face_damage_t[cd.c.face_1]-2),1.)*10.;
     if (cd.e1.imat ==0 ) damage_1 = 1.0 / damg;
     else damage_1 = 1.0;

     damg = max(double(cd.e2.face_damage_c[cd.c.face_2]
                                 + cd.e2.face_damage_t[cd.c.face_2]-2),1.)*10.;
     /* damg = (cd.e2.face_damage_c[cd.c.face_1] + cd.e2.face_damage_t[cd.c.face_1]-2)
            * dtseconds * damage_rate;  */
     if (cd.e2.imat ==0 ) damage_2 = 1.0 / damg;
     else damage_2 = 1.0;
  }
     else
  {
    damage_1 = 1.0;
    damage_2 = 1.0;
  }
  //double damage_1 = int(cd.e1.face_damage_c[cd.c.face_1]);
  //double damage_2 = int(cd.e2.face_damage_c[cd.c.face_2]);
  // result reduced rigidity (improve: comments 'приведенная жесткость'):
  // close-to-linear-spring rigidity of ice
  // CSD Let's try adding a weakening of the material strenth of an average after
  // it has broken.
  // CSD replaced by code in individual contact force models _edge_orient
  /* double weakening = 1;
  if (cd.c.type==ContType::COMPRESSIVE_FAILURE &&
      cd.c.failure_status == FailType::COMPRESSION) {
      weakening = 0.1;
  } */
  //CSD am trying out using damage_* info rather than nrm_weakening factor
  double H = h1*E1 * damage_1 * h2*E2 *damage_2
           / ( h1*E1 * abs( cd.r2 ) * damage_1 + h2*E2 * abs( cd.r1 ) * damage_2 );
  return ( isfinite( H ) ? H : 0.0 ) * cd.siku.planet.R_rec;
}

inline double _shear_mod( ContactData& cd )
{
  // ice thickness at largest (main) layer
  double h1 = cd.e1.h_main, h2 = cd.e2.h_main;

  // elasticity of elements
  double G1 = cd.siku.ms[cd.e1.imat].E/(2.0 * (1.0 + cd.siku.ms[cd.e1.imat].nu));
  double G2 = cd.siku.ms[cd.e2.imat].E/(2.0 * (1.0 + cd.siku.ms[cd.e2.imat].nu));
  double damage_1, damage_2;

  if (cd.e1.face_damage_s[cd.c.face_1]>1)
  {
     double dtseconds=cd.siku.time.get_dt();
     double damage_rate = 0.1;   //CSD 0.1 ~ 10 seconds to break a bond.
     /* double damg = (cd.e1.face_damage_s[cd.c.face_1]-1) * dtseconds * damage_rate;  */
     double damg = double(cd.e1.face_damage_s[cd.c.face_1]-1.)*10.;
     if (cd.e1.imat ==1 ) damage_1 = 1.0 / damg;
     else damage_1 = 1.0;

     /* damg = (cd.e2.face_damage_s[cd.c.face_1]-1) * dtseconds * damage_rate; */
     damg = double(cd.e2.face_damage_s[cd.c.face_1]-1.)*10.;
     if (cd.e2.imat ==1 ) damage_2 = 1.0 / damg;
     else damage_2 = 1.0;
  }
     else
  {
    damage_1 = 1.0;
    damage_2 = 1.0;
  }

  // result reduced rigidity (improve: comments 'приведенная жесткость'):
  // close-to-linear-spring rigidity of ice
  // CSD Let's try adding a weakening of the material strenth of an average after
  // it has broken.
  /* double weakening = 1;
  if (cd.c.type == ContType::COMPRESSIVE_FAILURE) {
      weakening = 0.1;
  } */
  //CSD am trying out using damage_* info rather than nrm_weakening factor
  double H = h1*G1*damage_1 * h2*G2*damage_2
           / ( h1*G1 * abs( cd.r2 ) + h2*G2 * abs( cd.r1 ) );
  return ( isfinite( H ) ? H : 0.0 ) * cd.siku.planet.R_rec;
}

// viscous and elastic forces applied to e1 caused by e2.
inline vec2d _elastic_force( ContactData& cd )
{
  vec2d norm; // direction of force applied to e1

  // IMPROVE: try to find better solution for normal direction search
  vec2d p1p2 [2];  // p1p2 = two points: p1 and p2
  size_t np = 0;  // amount of edge-edge intersections
  // noob search for p1 and p2
  for(size_t i = 0; i < cd.stats.size(); ++i )
    if( cd.stats[i] == PointStatus::EDGE )
      {
        if( np < 2 )  p1p2[ np ] = cd.interPoly[ i ];
        np++;
      }

  // direction of force

  if( np == 2 ) // definite front
    {
      // normal to front
      vec2d dp = p1p2[1] - p1p2[0];
      vec2d tau = ort( dp ) * copysign( 1., cross( p1p2[0], dp ) );
      norm = rot_90_ccw( tau );
    }
  else // no definite front
    {
      // normal is being calculated by interposition of polygons` centers and
      // intersection area center
      norm = ort(   ort( cd.r2 )*abs( cd.r1 )
                  - ort( cd.r1 )*abs( cd.r2 ) );
    }
  // vec2d norm2 = ort(cd.va12);


  //CSD account for initial collision zeroing
  double adj_area=max(cd.c.area - cd.c.init_area,0.0);

  // resulting force - close to linear spring
  return _rigidity( cd ) * (sqrt(adj_area)*1e-6 * cd.siku.planet.R2) * norm;
}

inline vec2d _elastic_force_new( ContactData& cd )
{
  vec2d norm; // direction of force applied to e1

  //CSD Edge contact points are found in the constructor now.
  // Assume that there are no collisions that are just at a point
  //CSD Following Feng, 2003, We should consider the possibility of multiple
  //CSD fronts, such as for elements that intersect on multiple edges, in complicated
  // ways. So for this really we want to sum up contributions from multiple contact
  // edges. For a 3 sided intersection polygon, we just need to use the pre-existing
  // procedure (find the vertices associated with the 'front' and face normal
  // associated with it). But for a 4,6, etc sided intersection poly we want to sum
  // up contributions from 'opposite' faces.
  vec2d dp;
  vec2d tau;
  int nev = cd.edge_inds.size();   //CSD the number of intersection polygon vertices
                                   // (labeled 'EDGE' in stats), that are on an edge
                                   // of of both the contacting elements, as opposed
                                   // to being on the interior of one of the elements.
  vec2d face_norm[nev]; // edge length times norm.
  vec2d face_norm_sum{0,0};
  if (nev>=2) {
      // CSD For collisions, specify the Force point as the midpoint of the
      // line between the edge points of the interpoly.
      vec2d p1p2[nev];
      vec2d mid_point[nev];
      //CSD the mid_point matters for getting the torque right. In principal
      // we need to apply each face_norm force at the correct mid_point of
      // the face segment. But we can't pass an array out of this routine
      // so for the time being we're summing up the force and applying it at
      // qthe intersection polygon center. An improvement would be to declare a
      // static vector of forces, return a pointer to it, and make sure to
      // apply the torque as a sum of torques at the right location. But for now
      // we will assume there's one major face_norm force, and the others are close
      // to round off (due to slightly off-center, shallow collisions). We will see
      // how far this gets us, but may need to return to this is we truly want to be
      // consistent for elements passing far into one another.
      for (int i=0;i<nev-1;i++) {
          p1p2[i] = cd.interPoly[cd.edge_inds[i]];  // interpoly edge points
      //    mid_point[i] = 0.5 * (cd.interPoly[cd.edge_inds[i]] +
      //            cd.interPoly[cd.edge_inds[i+1]]);
      }
      p1p2[nev-1] = cd.interPoly[cd.edge_inds[nev-1]];  // interpoly edge points
      //mid_point[nev-1] = 0.5 * (cd.interPoly[cd.edge_inds[0]] +
      //        cd.interPoly[cd.edge_inds[nev-1]]);
      //r1 = 0.5 * ( p1p2[0] + p1p2[1] );
      //r2 = r1 - r12;


      //vec2d p1p2[2];
      //p1p2[0] = cd.interPoly[cd.edge_inds[0]];  // p1p2 = two points: p1 and p2
      //p1p2[1] = cd.interPoly[cd.edge_inds[1]];  */

      // direction of force

      // normal to front
      //CSD if there are two points of intersection of the two polygons
      // the line connecting those two vertices represents the front
      for (int i=0;i<nev-1;i++) {
          dp = p1p2[i+1] - p1p2[i];
          tau = ort( dp ) * copysign( 1., cross( p1p2[i], dp ) );
          norm = rot_90_ccw( tau );
          face_norm[i] = abs(dp) * norm;
      }
      dp = p1p2[0] - p1p2[nev-1];
      tau = ort( dp ) * copysign( 1., cross( p1p2[nev-1], dp ) );
      norm = rot_90_ccw( tau );
      face_norm[nev-1] = abs(dp) * norm;

      // CSD if there are more than two interpoly vertices co-occuring on the edges of
      // the two elements, then the net force on an element is the sum over the
      // forces on the faces of that element. Effectively this means summing every
      // other face.
      // I'm pretty sure that this set of face normal choices
      // is a set of outward normals for element 1 of the contact.
      // I think this is what 'tau' above ensures.
      for (int i=0;i<nev;i+=2) {
          face_norm_sum = face_norm_sum + face_norm[i];
      }
  }
  /* else if (num_edges>2) {
      for (i==0;i<cd.stats.size(),i+=2) {

      }

  }*/
  /* else {         // no definite front
        //CSD it's possible that the two elements only intersect at a
        // single point, but I don't see this as a reason to abort,
        // just carry on.
       fputs("Single intersection point collision!!\n", stdout);
      abort();
  } */

  // vec2d norm2 = ort(cd.va12);
  //CSD we need to ignore area estimates that are on the scale
  // of the area estimate difference between area and area2
  double adj_area=max(cd.c.area - cd.c.init_area,0.0);
                      //- 2.0* cd.area_diff,0.0);
  if (nev<2) {
      adj_area = 0.0;
  }
  //CSD account for initial collision zeroing ??
  // double adj_width=max(abs(dp) - cd.c.init_width,0.0);

  // resulting force -
  return _rigidity( cd ) * (sqrt(adj_area)
                         * cd.siku.phys_consts["gamma"]
                         * cd.siku.planet.R2) * face_norm_sum;
}

inline vec2d _viscous_force( ContactData& cd )
{

//   double adj_area=max(cd.c.area - cd.c.init_area,0.0);
   double adj_area=max(cd.c.area - cd.c.init_area,0.0);
                      //- 2.0* cd.area_diff,0.0);

  return - (adj_area * cd.siku.planet.R2) * cd.siku.phys_consts["etha"] * cd.va12;
}

// -----------------------------------------------------------------------

inline void _fasten( ContactData& cd )
{
  // if at least one element is shore (static but not fastened):
    if (cd.c.type==ContType::COLLISION || cd.c.type == ContType::INIT_COLLISION) {
        if (cd.inter_res > 2 )  {   //CSD if the elements overlap
            if( (( cd.e1.flag & Element::F_STATIC) !=      //CSD and one is static or static slip
                 ( cd.e2.flag & Element::F_STATIC)) ||
                 (( cd.e1.flag & Element::F_STATIC_SLIP) !=
                 ( cd.e2.flag & Element::F_STATIC_SLIP)))   {
                // minimal areas for comparison
                double am = min( cd.e1.A, cd.e2.A );
                cd.e1.Amin = min( cd.e1.Amin, am );
                cd.e2.Amin = min( cd.e2.Amin, am );
                // Not sure why we are time integrating the overlap area here...
                cd.e1.OA += cd.area;
                cd.e2.OA += cd.area;
                //CSD compare the overlap areas and make element static if it overlaps too much.
                // CSD do we really need the 'FASTENED' classification?
                if (cd.area > 0.10*am) {
                    // greater than 10% overlap make whichever element is not static, static
                    int overlap = 1;
                    // ...we can determine which of the two is not static and make it static...
                    // but there's no reason at this point not to set both to static
                    // we'd never want to set the grounded ice element to static_slip, because that
                    // would break any offshore bonds the element has.
                    //cd.e1.flag |= Element::F_STATIC;
                    //cd.e2.flag |= Element::F_STATIC;
                }
            }
        }
    }
}

inline void _apply_interaction( ContactData& cd, InterForces& if_ )
{
  // forces applied to e1 and e2 in their local coords (SI)
  //CSD as a check let's record the values of some of the contact related vectors
  //cd.c.F2d = if_.F1;
  cd.c.r1  = if_.rf1;
  //cd.c.crc  = cross( if_.rf2, if_.F2 );
  // cd.c.crc = dot(cd.va1r-cd.va2r,if_.rf1);
  //cd.c.crc = dot(cd.va1r, cd.va2r);
  vec3d F1 = vec2_to_vec3_s( if_.F1 ),
        F2 = lay_on_surf( cd.e1_to_e2 *
             vec2_to_vec3_s( if_.F2) );
  //vec3d F1 = vec2_to_vec3_s( Ftv1 ),
  //      F2 = lay_on_surf( cd.e1_to_e2 * vec2_to_vec3_s( Ftv2 ) );
    // torques (combined) applied to e1 and e2 in their local coords (SI)

  double tq1 = cd.siku.phys_consts["rotatability"] * if_.couple1
             + cross( if_.rf1, if_.F1 )
             * cd.siku.phys_consts["rotatability"];
  double tq2 = cd.siku.phys_consts["rotatability"] * if_.couple2
             + cross( if_.rf2, if_.F2 )
             * cd.siku.phys_consts["rotatability"];

  // TODO: find and clean (set properly) all adjustment factors like below

//#pragma omp critical
  {
  cd.e1.F += F1;
  cd.e1.N += tq1;

  cd.e2.F += F2;
  cd.e2.N += tq2;
  }

  // ------------ stress tensor components calculation ----------------------
 // CSD We need an alternative for calculating stresses in the 'collision' case
 // because v[12][12] aren't assigned, nor necessarily accurate if they were assigned
 // So let's make an estimate of the v-coordinates from the 'diagonal' of the interpoly
 // or at least what we will call a diagonal
 vec3d v11, v12, v21, v22;
 vec2d n1, n2;

 if( cd.c.type == ContType::JOINT
  || cd.c.type == ContType::COMPRESSIVE_FAILURE
  || cd.c.type == ContType::SHEAR_CMP_FAILURE)   {
      v11 = vec2_to_vec3_s( vec3_to_vec2( cd.e1.P[cd.c.v11] ) );
      v12 = vec2_to_vec3_s( vec3_to_vec2( cd.e1.P[cd.c.v12] ) );
      v21 = vec2_to_vec3_s( vec3_to_vec2( cd.e2.P[cd.c.v21] ) );
      v22 = vec2_to_vec3_s( vec3_to_vec2( cd.e2.P[cd.c.v22] ) );
      n1 = orthog(cd.ed1);
      n2 = orthog(cd.ed2);
 }
 else if( cd.c.type == ContType::COLLISION)   {
      vec2d ip_vec2, best_face;
      double best_est=1.1;
      double perp_est;
      int ib, jb;
      // find estimate of 'perpindicular to contact' for e1
      for (int i=0; i < cd.inter_res-1; i++) {
          for (int j=i+1; j < cd.inter_res; j++) {
            ip_vec2.setx(cd.interPoly[j].x-cd.interPoly[i].x);
            ip_vec2.sety(cd.interPoly[j].y-cd.interPoly[i].y);
            perp_est=abs(dot(ort(cd.r1),ort(ip_vec2)));
            if (perp_est<best_est) {
                best_est=perp_est;
                ib=i; jb=j;
            }
          }
      }
      ip_vec2.setx(cd.interPoly[jb].x-cd.interPoly[ib].x);
      ip_vec2.sety(cd.interPoly[jb].y-cd.interPoly[ib].y);
      v11 = vec2_to_vec3_s(cd.interPoly[jb]);
      v12 = vec2_to_vec3_s(cd.interPoly[ib]);
      /*CSD get orientation of polygon edge relative to element consistent with
        ed1 calculation */
      n1 = orthog(- unit((cd.interPoly[jb] - cd.interPoly[ib]) *
           cross(-cd.interPoly[ib],cd.interPoly[jb]-cd.interPoly[ib])));
      // repeat for e2 using the polygon in the e2 coordinates.
      best_est=1.1;
      for (int i=0; i < cd.inter_res-1; i++) {
          for (int j=i+1; j < cd.inter_res; j++) {
            ip_vec2.setx(cd.interPoly2[j].x-cd.interPoly2[i].x);
            ip_vec2.sety(cd.interPoly2[j].y-cd.interPoly2[i].y);
            perp_est=abs(dot(ort(cd.r2),ort(ip_vec2)));
            if (perp_est<best_est) {
                best_est=perp_est;
                ib=i; jb=j;
            }
          }
      }
      ip_vec2.setx(cd.interPoly2[jb].x-cd.interPoly2[ib].x);
      ip_vec2.sety(cd.interPoly2[jb].y-cd.interPoly2[ib].y);
      v21 = vec2_to_vec3_s(cd.interPoly2[jb]);
      v22 = vec2_to_vec3_s(cd.interPoly2[ib]);
      n2 = orthog(- unit((cd.interPoly2[jb] - cd.interPoly2[ib]) *
                  cross(-cd.interPoly2[ib],cd.interPoly[jb]-cd.interPoly2[ib])));
 }
 // CSD really nothing useful is done as a result of the else below
 // should probably skip below calculations in this case.
 else {
     v11 = vec2_to_vec3_s( vec3_to_vec2( cd.e1.P[cd.c.v11] ) );
     v12 = vec2_to_vec3_s( vec3_to_vec2( cd.e1.P[cd.c.v12] ) );
     v21 = vec2_to_vec3_s( vec3_to_vec2( cd.e2.P[cd.c.v21] ) );
     v22 = vec2_to_vec3_s( vec3_to_vec2( cd.e2.P[cd.c.v22] ) );
     n1 = orthog(cd.ed1);
     n2 = orthog(cd.ed2);
}

  // CSD I want n1 and n2 to point toward the interior of the respective elemetns.
 // CSD the cross cross combination below doesn't guarantee that.
 //  vec2d n1 = ort( vec3_to_vec2_s( cross( cross( v11, v12 ), (v11 - v12) ) ) );
 //  vec2d n2 = ort( vec3_to_vec2_s( cross( cross( v21, v22 ), (v21 - v22) ) ) );
  /* vec2d n1 = orthog(cd.ed1);
  vec2d n2 = orthog(cd.ed2);  */


  vec2d ex { 1., 0. }, ey { 0., 1. };

  double l1 = abs( v11 - v12 ) * cd.siku.planet.R,
         l2 = abs( v21 - v22 ) * cd.siku.planet.R;

  if( l1 < 1e-12 || l2 < 1e-12 ) return;


  // CSD I think the orientation of the n1 and n2 vectors  depends
  // CSD on which pair of vertices in a joint happen to be closest
  // CSD so the sign of the stress components can flip flop if
  // CSD the stretch between vertices is very close.
  // CSD I think we only need the magnitude from the n[12] variables
  // CSD not the sign, so in the n[12][xy] variables below
  // CSD I use the absolute value of the dot product.
  // CSD check that, we DO need the sign so that n1 points towards the element
  // CSD center.  With my approach here, I need to divide the stresses by 2, due
  // CSD to the way we are doing the summation.
  // CSD I will implement this by adding the factor to d1 and d2

  // CSD I'm still puzzled by the orientation of the vectors here
  // CSD based on the way I'm orienting the coordinate system
  // CSD it seems that in order for a compressive force to be associated
  // CSD with a positive normal stress, a negative sign needs to be
  // CSD added in the f*x and f*y calculations below.
  double f1x = - dot( vec3_to_vec2_s(F1), ex ),  //CSD changed sign LHS
         f1y = - dot( vec3_to_vec2_s(F1), ey ),  //CSD changed sign LHS
         n1x = dot( n1, ex ),
         n1y = dot( n1, ey ),
         d1  = cd.e1.h_main * l1 * 2.0,   // CSD factor of 2 added here..

         f2x = - dot( vec3_to_vec2_s(F2), ex ),  //CSD changed sign LHS
         f2y = - dot( vec3_to_vec2_s(F2), ey ),  //CSD changed sign LHS
         n2x = dot( n2 ,ex ),
         n2y = dot( n2, ey ),
         d2  = cd.e2.h_main * l2 *2.0;

//#pragma omp critical
  // CSD If elements are fixed or steady assume there is an opposing stress
  // CSD on the opposite sides that counter the applied stresses.

      if ( cd.e1.flag & Element::F_STATIC ||
           cd.e1.flag & Element::F_STATIC_SLIP ||
           cd.e1.flag & Element::F_STEADY ||
           cd.e1.flag & Element::F_ANAVEL)
      {
         cd.e1.Sxx += 2*f1x * n1x / d1;
         cd.e1.Syy += 2*f1y * n1y / d1;
         cd.e1.Sxy += 2*f1x * n1y / d1;   // CSD sign change
         cd.e1.Syx += 2*f1y * n1x / d1;
      }
      else
      {
         cd.e1.Sxx += f1x * n1x / d1;
         cd.e1.Syy += f1y * n1y / d1;
         cd.e1.Sxy += f1x * n1y / d1;   // CSD sign change
         cd.e1.Syx += f1y * n1x / d1;
      }

      if ( cd.e2.flag & Element::F_STATIC ||
           cd.e2.flag & Element::F_STEADY ||
           cd.e2.flag & Element::F_ANAVEL)
      {
           cd.e2.Sxx += 2 * f2x * n2x / d2;
           cd.e2.Syy += 2 * f2y * n2y / d2;
           cd.e2.Sxy += 2 * f2x * n2y / d2;  // CSD sign change
           cd.e2.Syx += 2 * f2y * n2x / d2;
      }
      else
      {
          cd.e2.Sxx += f2x * n2x / d2;
          cd.e2.Syy += f2y * n2y / d2;
          cd.e2.Sxy += f2x * n2y / d2;  // CSD sign change
          cd.e2.Syx += f2y * n2x / d2;
      }

}

inline void _update_contact( ContactData& cd )
{
  // Hopkins physics special case
  if( cd.siku.cont_force_model == CF_HOPKINS )
    {
      // durability depends on the number of broken integral parts
      cd.c.durability -= (1. - cd.d1); // additive
      //cd.c.durability *= cd.d1; // or multiplicative

      if(cd.c.durability < cd.siku.phys_consts["dest_threshold"])
        cd.c.durability = 0.;
    }
  else if ( cd.siku.cont_force_model == CF_DIST_SPRINGS )
    {
      // durability change - joint destruction
      double r_size = cd.siku.planet.R_rec / cd.c.init_len,// reversed size (SI)
             dmax   = max( cd.d1, cd.d2 ),                 // maximal stretch
             dave   = (cd.d1 + cd.d2) * 0.5;                  // average stretch
             //dave   = sqrt(cd.d1 * cd.d2);               // average stretch
             //dave   = (cd.d1 * cd.d2) / (cd.d1 + cd.d2); // average stretch

      double sigma   = cd.siku.phys_consts["solidity"],
             epsilon = cd.siku.phys_consts["tensility"];

      // TODO: discuss time scaling
      cd.c.durability -= ( dmax * r_size > epsilon ) ?
          //dave * r_size * cd.siku.time.get_dt() * sigma     :       0.;
          dave * r_size     :       0.;
    }
  else if ( cd.siku.cont_force_model == CF_WILCH_LIKE ||
            cd.siku.cont_force_model == CF_EDGE_ORIENT)
  {
      double sig_max_c   = cd.siku.ms[0].layers[0].sigma_c,
             sig_max_t = -cd.siku.ms[0].layers[0].sigma_t,
             coef_fric = cd.siku.ms[0].mu;

/*      cd.c.durability -= (cd.c.Sn > sig_max_c *1000 || cd.c.Sn < sig_max_t * 1000) ?
                         0.1 : 0;
      cd.c.durability -= (abs(cd.c.Ss) > coef_fric * (cd.c.Sn - sig_max_t * 1000)) ?
                         0.1 : 0;
*/

      int damage_flag_1=1-(cd.e1.flag & Element::F_STATIC ||
                       cd.e1.flag & Element::F_CONTROLLED ||
                       cd.e2.flag & Element::F_CONTROLLED ||
                       cd.e1.flag & Element::F_STATIC_SLIP);
      int damage_flag_2=1 -(cd.e2.flag & Element::F_STATIC ||
                       cd.e1.flag & Element::F_CONTROLLED ||
                       cd.e2.flag & Element::F_CONTROLLED ||
                       cd.e2.flag & Element::F_STATIC_SLIP);

      if (cd.c.type==JOINT) {
          if (cd.c.Sn > sig_max_c *1000) {
              cd.c.durability -= 0.1;     // CSD for now drop the durability gradually ovef 100 timesteps
              cd.c.failure_status = COMPRESSION;
              cd.e1.face_damage_c[cd.c.face_1]+=1*damage_flag_1;
              cd.e2.face_damage_c[cd.c.face_2]+=1*damage_flag_2;
              //          cd.c.failure_status = FailType::COMPRESSIVE
          }
          if (cd.c.Sn < sig_max_t * 1000) {
              cd.c.durability -= 0.1;
              cd.c.failure_status = TENSION;
              cd.e1.face_damage_t[cd.c.face_1]+=1*damage_flag_1;
              cd.e2.face_damage_t[cd.c.face_2]+=1*damage_flag_2;
          }
          if (abs(cd.c.Ss) > coef_fric * (cd.c.Sn - sig_max_t * 1000)) {
              cd.c.durability -= 0.1;
              cd.c.failure_status = SHEAR;
              cd.e1.face_damage_s[cd.c.face_1]+=1*damage_flag_1;
              cd.e2.face_damage_s[cd.c.face_2]+=1*damage_flag_2;
          }
      }
      cd.c.durability = max(cd.c.durability,0.01);
  }
  /* else if ( cd.siku.cont_force_model == CF_EDGE_ORIENT )
  {
      double del_max_c   = cd.siku.phys_consts["solidity"],
             del_max_t = -cd.siku.phys_consts["tensility"];
      double r_size = cd.siku.planet.R_rec / cd.c.init_len;// reversed size (SI)
      // CSD for now add shear/normal strain based failure criteria here
      double del_shear1=abs(cd.dr1 * cd.ed1);
      double del_comp1=cd.dr1 * orthog(cd.ed1);
      double del_shear2=abs(cd.dr2 * cd.ed1);
      double del_comp2=cd.dr2 * orthog(cd.ed1);
     // CSD Compare with failure criteria for first vertices
     // Consider both tensile and compressive failure,
     // This failure can be a function of time if for example
     // fine scale cracks accumulate under a limiting load before the
     // actual failure occurs.  For now I will make the joint decay
      // over 10 timesteps, due to a tensile or compressive failure
      cd.c.durability -= ( (del_comp1+del_comp2)/2 * r_size > del_max_c
                           || (del_comp1+del_comp2)/2 * r_size < del_max_t) ? 0.1 : 0.;
     // CSD try a Mohr-Coulomb style condition on the shear strain as well
     // CSD here I'm checking if the principal stresses on the associated elements
     // exceed the Mohr-Coulomb criteria, and if the face orientation is in
     // proximity to the plane of maximum shear.
      double e1_mc = (abs(cd.e1.S1-cd.e1.S2) > (cd.e1.S1+cd.e1.S2) * sin(15/180*3.1415)
                      + 1e6 * cos(15/180*3.1415)) ? 1.0 : 0.0;
      double e2_mc = (abs(cd.e2.S1-cd.e2.S2) > (cd.e2.S1+cd.e2.S2) * sin(15/180*3.1415)
                      + 1e6 * cos(15/180*3.1415)) ? 1.0 : 0.0;                                         ;
      vec2d e1_sangle{cos(cd.e1.shear_angle), sin(cd.e1.shear_angle)};
      vec2d e2_sangle{cos(cd.e2.shear_angle), sin(cd.e2.shear_angle)};
      double face1_sh = dot(e1_sangle, orthog(cd.ed1));
      double face2_sh = dot(e2_sangle, orthog(cd.ed2));

      cd.c.durability -= 0.1 * e1_mc *e2_mc ;
  }  */

#pragma omp critical
  {
  // check for errors
  if( errored( cd.loc_P1 ) )   cd.e1.flag |= Element::F_ERRORED;
  if( errored( cd.loc_P2 ) )   cd.e2.flag |= Element::F_ERRORED;

  // land fastening
  //CSD The land fastening idea is kinda dumb and can lead to a chain reaction
  // spreading static elements through the domain. If we don't calculate OA  the
  // fastency check in mproperties never happens and ice doesn't become static
  // by grinding up against other static elements for too long.
  /* _fasten( cd ); */
  }

//// Deprecated: replaced by other mechanism
// may be required in 'collision' contact type
//      if( c.durability < 0.05 )
//        {
//          std::vector<vec2d> loc_P1;  // e1.P vertices in local 2d coords
//          std::vector<vec2d> loc_P2;  // e2.P vertices in local 2d coords
//
//          // polygons in local (e1) coords
//          for( auto& p : e1.P )
//            loc_P1.push_back( vec3_to_vec2( p ) );
//          for( auto& p : e2.P )
//            loc_P2.push_back( vec3_to_vec2( e2_to_e1 * p ) );
//
//          if( errored( loc_P1 ) )   e1.flag |= Element::F_ERRORED;
//          if( errored( loc_P2 ) )   e2.flag |= Element::F_ERRORED;
//
//          intersect( loc_P1, loc_P2, interPoly, nullptr, nullptr, &area );
//
//          c.area = area;
//        }
}

// =========================== contact force ================================

void contact_forces( Globals& siku )
{
//# ifdef SIKU_OPENMP
//# pragma omp parallel for //num_threads(4) // without 'n_t()' - auto-threading
//# endif
  for( int i = 0; i < siku.ConDet.cont.size(); i++ )
//  for( auto& c : siku.ConDet.cont ) // reorganized for OpenMP
    {
      auto& c = siku.ConDet.cont[i];

      // conditional cancellation of interaction
      if(
          // TODO: such errors should be removed by removing their reason
          //(siku.es[c.i1].flag & Element::F_ERRORED) ||
          //(siku.es[c.i2].flag & Element::F_ERRORED) ||
      // No need to calculate interaction for two steady polygons
      // TODO: reconsider runtime fastened ice
          ( (siku.es[c.i1].flag & Element::F_STEADY) &&
            (siku.es[c.i2].flag & Element::F_STEADY) ) )
            // CSD maybe something similar for ANAVEL?  but something funny happens here if I try. contact forces shut off between free and steady.
 //         ( (siku.es[c.i1].flag & Element::F_ANAVEL) &&
 //           (siku.es[c.i2].flag & Element::F_ANAVEL) )  )
          continue;

      // calculation of elements inter-section, -position, -velocity and
      // some additional parameters
      ContactData cd( c, siku );

      //CSD The area field of 'c' is not used anywhere in the code currently
      //CSD It would be nice to carry the interpoly area in this variable
      //CSD so when contacts are cleared, we could properly label a joint
      //CSD as a compression break only so long as it remains in compression after
      //CSD a compressive failure.
      //CSD ...not completely sure why we have cd.area and cd.c.area, that's
      //CSD probably something that needs cleaning up.
      c.area=cd.area;

      /*vec2d chk_c1= c.va12_cb[1];
      vec2d chk_c2= c.va12_cb[2];
      vec2d chk_c3= c.va12_cb[3];
      vec2d chk_c4= c.va12_cb[4];
      vec2d chk_c5= c.va12_cb[5];
      vec2d chk_c6= c.va12_cb[6];*/
      InterForces intf{};  // elements` interaction forces
      // InterForces intf1;//////TEST: second set of forces for accumulating
                        // both types of interaction
/////////////////////////////////////////////////

      // calculating the forces
      if( c.type != ContType::JOINT
       && c.type != ContType::COMPRESSIVE_FAILURE
       && c.type != ContType::SHEAR_CMP_FAILURE)
        {
          //if( c.type == ContType::COLLISION )
          //CSD There's no reason to calculate a collision interaction if the
          //CSD intersection area is zero. Comparison with adj area is within
          //CSD _collision so that those contacts are cleared only when
          //CSD elements are no longer 'colliding' numerically.
          if ( c.area-c.init_area >= 0.0 ) {
             intf = _collision( cd ); // collision forces
          }
          else continue;
        }
      else if( c.durability <= 0. )
        {
          // IMPROVE: find this leak in 'update' function and prevent the
          // possibility of such contacts
          //intf = {};  // by default
        }
      else
        {
          // TODO: reconsider manual optimization: single switch on loading
          // combined with calling function by pointer on calculation time.
          switch( siku.cont_force_model )
          {
            case CF_TEST_SPRINGS: //same as CF_DEFAULT
              intf = _test_springs( cd );
              break;

            case CF_HOPKINS:
              intf = _hopkins_frankenstein( cd );
              break;

            case CF_DIST_SPRINGS:
              intf = _distributed_springs( cd );
              break;

            case CF_WILCH_LIKE:
              intf = _wilch_like( cd );
              break;

            case CF_EDGE_ORIENT:
              intf = _edge_orient( cd );
              break;

          }
        }

      // accumulating forces and torques applied to elements
      _apply_interaction( cd, intf );

      // check of contact destruction/renovation, land-fastening conditions,
      // e.t.c.
      _update_contact( cd );

    }
}

// ============================= definitions ================================

InterForces _collision( ContactData& cd )
{
  InterForces if_{};

  if( cd.inter_res > 2 )
    {
      // CSD don't overwrite BROKEN_JOINT type as collision
      /* if (cd.c.type != ContType::COMPRESSIVE_FAILURE)  */
      // CSD distinguish collisions in the initial state from 'real' collisions //
      // CSD any initial collision area is zeroed and the inital area of
      //  intersection is preserved to zero out.
      if (cd.siku.time.get_n()==0)
      {
          cd.c.type = ContType::INIT_COLLISION;
          cd.c.init_area = cd.area;
      }
      //CSD when a bond breaks it is possible for the collision estimate to suggest
      // compression, while the joint estmiate was just considering the joint to be in
      // a tensile state. In this case we want no elastic normal force added, so we zero-out
      // the 'initial' area of overlap between elements.
      if (cd.c.Sn < 0) cd.c.init_area = max(cd.area,0.0);

      // CSD if the area of overlap between elements diminishes. diminish the
      // overlap-area correction until zero.
   /*   cd.c.init_area = min(cd.c.init_area, cd.area); */

      // if (cd.area==0) cd.c.init_area = 0;
      // CSD should probably double check that this next line is necessary, but ]
      // I must have put it here for a reason.
      if (cd.c.init_area==0) cd.c.type = ContType::COLLISION;

      double adj_area=max(cd.c.area - cd.c.init_area,0.0);

      // force in Newtons applied to e1 caused by e2
      // vec2d F = _elastic_force( cd ) * cd.siku.phys_consts["rigidity"];
      // CSD for an energy conserving collision, we can't use the
      // intersection area in our estimate of the force because it is not
      // a potential field. Instead we use the width of the intersection
      vec2d F = _elastic_force_new( cd ) * cd.siku.phys_consts["rigidity"];


      //CSD a temporary recording of the unit normal to the elastic force
      //CSD direction - it looks like this is identical to c1, so perhaps
      //CSD all the calcs below related to c1 are unnecessary.
      vec2d f1 = rot_90_ccw(unit(F));
      // IMPROVE: reconsider 'rigidity' and 'viscosity' sense

      //CSD I need a way of forcing consistency between the normal force
      // before and after joint failure  for the case where there is a shear
      // failure in compression
      // CSD here I'm using durability as an indicator that a joint just broke.
      // So I need to reset durability of all contacts that are collisions after
      // the first pass through of this so init_area doesn't get repeatedly reset.

      // NOTE: We still aren't dealing with compressive failure explicitly.
      if (cd.c.durability<=0.01 && cd.c.Sn>0 && cd.c.failure_status == SHEAR)    // If a bond in compression just broke ...
      {
          cd.c.init_area = cd.area * (1.0 - cd.c.F_elj.x * cd.siku.phys_consts["rigidity"] / F.x);
          F = {cd.c.F_elj.x, 0.};
      }

      cd.c.durability = 1.0;
      // renewing the contact to avoid deletion
      cd.c.generation = 0;

      //CSD calculate shear and normal stresses as consistent as possible with other contact methods
      vec2d ip_vec1;
      // double best_est=1.1;
      // double ip_mag;
      double longest = 0;
      int ib, jb;
      double ip_mag;

      // CSD Let me try using an edge_l that is consistent with
      // the 'front' estimate in _elastic_force
      // IMPROVE: try to find better solution for normal direction search
      vec2d p1p2 [2];  // p1p2 = two points: p1 and p2
      size_t np = 0;  // amount of edge-edge intersections
      // noob search for p1 and p2
      for(size_t i = 0; i < cd.stats.size(); ++i ) {
        if( cd.stats[i] == PointStatus::EDGE )
          {
            if( np < 2 )  p1p2[ np ] = cd.interPoly[ i ];
            np++;
          }
      }
      // find estimate of 'perpindicular to contact' for e1
      // CSD to do this lets assume that the contact face
      // is oriented with the longest interpoly bisector.
      for (int i=0; i < cd.inter_res-1; i++) {
          for (int j=i+1; j < cd.inter_res; j++) {
            ip_vec1.setx(cd.interPoly[j].x-cd.interPoly[i].x);
            ip_vec1.sety(cd.interPoly[j].y-cd.interPoly[i].y);
            ip_mag=abs(ip_vec1);
            if (ip_mag>longest) {
                longest=ip_mag;
                ib=i; jb=j;
            }
          }
      }
      ip_vec1.setx(cd.interPoly[jb].x-cd.interPoly[ib].x);
      ip_vec1.sety(cd.interPoly[jb].y-cd.interPoly[ib].y);
      // CSD in order to determine the collision face ..
      // CSD compare the position of the intersection polygon centroid
      // CSD to the centerpoint of each e1 polygon side

      // cvpoly2d intrsec_poly=cvpoly2d(cd.interPoly);
      // vec2d ipoly_cntr = intrsec_poly.center();
      vec2d seg_cntr=0.5*(cd.loc_P1[cd.loc_P1.size()-1]+cd.loc_P1[0]);
      double dist1 = abs(seg_cntr-cd.icen);
      cd.c.face_1 = cd.loc_P1.size()-1;  // asssume the last polygon face is the closest
      for (int ip=0; ip < int(cd.loc_P1.size()-1); ip++)
      {
          seg_cntr=0.5*(cd.loc_P1[ip]+cd.loc_P1[ip+1]);
          if (abs(seg_cntr-cd.icen) < dist1)
          {
              cd.c.face_1 = ip;
              dist1=abs(seg_cntr-cd.icen);
          }
      }
      //CSD repeat operation for second polygon
      // intrsec_poly=cvpoly2d(cd.interPoly2);
      // ipoly_cntr = intrsec_poly.center();
      seg_cntr=0.5*(cd.loc_P2[cd.loc_P2.size()-1]+cd.loc_P2[0]);
      double dist2 = abs(seg_cntr-cd.icen2);
      cd.c.face_2 = cd.loc_P2.size()-1;  // asssume the last polygon face is the closest
      for (int ip=0; ip < int(cd.loc_P2.size()-1); ip++)
      {
          seg_cntr=0.5*(cd.loc_P2[ip]+cd.loc_P2[ip+1]);
          if (abs(seg_cntr-cd.icen2) < dist2)
          {
              cd.c.face_2 = ip;
              dist2=abs(seg_cntr-cd.icen2);
          }
      }


      double edge_l = longest;
      double edge_l2 = abs(p1p2[0]-p1p2[1]);
  //    vec2d const c1 = orthog(- unit((cd.interPoly[jb] - cd.interPoly[ib]) *
  //          cross(-cd.interPoly[ib],cd.interPoly[jb]-cd.interPoly[ib])));  // approximate collision surface
  // CSD this cross product should make sure that the face orientation
  // is consistent relative to the center of the e1 reference frame
      double orient=cross(cd.interPoly[ib],cd.interPoly[jb]);
      double or_sign = (0<orient) - (0 > orient);
      vec2d const c1 = - or_sign * unit(cd.interPoly[jb] - cd.interPoly[ib]);

  //     cd.ed1 = - unit(p1 - p2) * copysign(1.0, cross(-p2, p1-p2));  // CSD neg. if p2 is CW of p1

      vec2d const c2 = orthog(c1);   // directed at COM
      // vec2d const f1 = unit(cd.ed1);  // tangent to face
      // vec2d const f2 = unit(rot_90_ccw(cd.ed1));  // normal to face inward pointin
      // CSD calculate 'stresses' ....this includes viscous and frictional contributions.
      // CSD Need to check this, I think it should include ice thickness in denominator
      // CSD for now we are just assuming 1m thick ice, so are 'okay'.
      // CSD for the edge length here I want to use init_wid the full width of the contact edge
      // CSD rather than the edge_l of the contact, but sometimes edge_l is all we have (no previously existing joint).
      double inv_sec = 1. / (max(cd.c.init_wid,edge_l) * cd.siku.planet.R);
      cd.c.Sn = inv_sec * dot(F, c2);
      cd.c.Ss = inv_sec * dot(F, c1);
      //CSD approximate the energy of the collision as a strain energy
      //cd.c.W_strain = 0.5 * abs(F) * adj_area / edge_l * cd.siku.planet.R;
      cd.c.W_strain = 2./3. * _rigidity( cd )
                            * cd.siku.phys_consts["gamma"]
                            * pow(adj_area,1.5) * pow(cd.siku.planet.R,3) ;
      cd.c.W_couple = 0.5 * cross( cd.c.r1, -F );
      //cd.c.crc =  adj_area;
      // cd.c.W_strain = abs(F);
      // CSD add a frictional force along the face direction of
      // the collision.  This is proportional to the area of overlap
      // (normal stress) divided by the length of the overlap and the rate at
      // which the elements are moving relative to each other
      //CSD we might want to make this a material property ,or a function of
      // how the material fails?
      // double fric_dissp = cd.siku.phys_consts["fric_dissp"];

      // CSD odd first-shot attempt at a frictional tangential force
      /* F -= fric_coef
          *(adj_area / edge_l * cd.siku.planet.R2)
          * dot(cd.va12,c1) * c1;  */
      // CSD a more reasonable coulomb friction. This acts when two elements are moving relative to each other
      // I haven't yet figured out a way to implement a truly 'static' friction since it's the motion of the elements
      // that determines the displacements which determine the forces.

      //vec2d F_fric = fric_coef * dot(F, c2)
      //               * dot(cd.va12, c1) * c1;

      //Neither of these works that well.  We want this to be friction-like and not viscous as much as possible
      // That is I want it to keep the two blocks motionless as much as possible until a static friction value is
      // reached.  We can estimate the force necessary to bring the current v12 || (parallel) to zero using the time
      // step and the element mass. And we use a friction coefficient and the normal force to estimate the max static
      // Initially I tried doing this with va12 but it appeared to be numerically unstable so I have inplemented
      // a circle_buffer to hold the last 6 values, which will be averaged for the f_v_negate estimate.

      // Ummm no...not right at all. Typically in DEM we want to let unjoined blocks slide freely until there
      // is a normal force adequate to cause frictional attenuation
      //CSD push current value of va12 onto the buffer
      cd.c.va12_cb.push_back(cd.va12);
      vec2d va12_avg={0.,0.};

      for (int i=0; i<cd.c.va12_cb.size(); i++) {
          va12_avg += cd.c.va12_cb[i];
      }
      va12_avg /=cd.c.va12_cb.size();
      double f_v_negate = abs( dot(va12_avg, c1) / cd.siku.time.get_dt()
                             * min (cd.e1.m, cd.e2.m));
      // If the blocks are in relative motion and there is a compressive force on the face, apply a force to inhibit
      // the motion up to but not exceeding the coulomb friction
      vec2d F_fric_adj{0.,0.};
      double f_fric_max = max(cd.siku.phys_consts["fric_dissp"] * dot(F, c2),0.);
      //if (cd.siku.phys_consts["fric_dissp"] > 0 & abs(dot(F,c1)) > abs( f_fric_max ) & abs( dot( cd.va12,c1 )) > 0)
      if ((cd.siku.phys_consts["fric_dissp"] > 0) & (abs(dot(cd.va12,c1)) > 0))
      {
          F_fric_adj = copysign(1.0, dot(cd.va12, c1)) * min(f_fric_max, f_v_negate) * c1;
      }
      //vec2d fvn = rot_90_ccw(unit(f_v_negate));
      //vec2d ffm = rot_90_ccw(unit(f_fric_max));

      F -= F_fric_adj;

      cd.c.Ss += inv_sec * dot(F_fric_adj, c1);
      // CSD calculate the change in frictional energy over the timestep. Here I am
      // assuming (?) that va12 is in m/s and get_dt gives seconds. Note that unlike
      // W_strain, this should give the amount of energy absorbed through friction over
      // the time step, not the total energy given the spring state, or mass and velocity.
      // For now we are making the approximation that the current velocity is a reasonable estimate
      // of the avg velocity since the last save point. We are saving the total Frictional energy dissipation
      // since the last write. - thus the dts in the calculation below.

      //cd.c.W_fric = abs(F_fric_adj) * abs(dot(cd.va12, c1)) * cd.siku.time.get_dts();
      cd.c.W_fric = abs(F_fric_adj) * abs(dot(va12_avg, c1)) * cd.siku.time.get_dts();

      F += _viscous_force( cd ) * cd.siku.phys_consts["viscosity"];
      // couple caused by friction
      double vt =  (cd.e2.W.z - cd.e1.W.z) * cd.siku.phys_consts["etha"]
                   * pow( (adj_area * cd.siku.planet.R2), 2 ) / ( 6. * M_PI );

      /* CSD Calculate factors that measure how far the stress at the contact
         is from a specified stress state for control elements. If the elements are in collision contact
         we just want to resist */
      if (cd.e1.flag & Element::F_CONTROLLED || cd.e2.flag & Element::F_CONTROLLED) {
          double lim_prox_n, lim_prox_s;
          vec2d const ex { 1., 0. }, ey { 0., 1. };
          double brf = cd.siku.phys_consts["bnd_resist_fac"];
          if (cd.c.Sn>0) {
             lim_prox_n = min(
                              max(cd.siku.ms[0].layers[0].sigma_c *1000-cd.c.Sn*brf,0.0)
                              /(cd.siku.ms[0].layers[0].sigma_c *1000)
                              ,1.0);
          }
          else lim_prox_n = 1.0;

          if (abs(cd.c.Ss)>0)
          {
              lim_prox_s = min(
                           max(cd.siku.ms[0].mu*cd.siku.ms[0].layers[0].sigma_t*1000
                           - abs(cd.c.Ss),0.0)
                           /(cd.siku.ms[0].mu*cd.siku.ms[0].layers[0].sigma_t *1000)
                           ,1.0);
          }
          else lim_prox_s = 1.0;

          vec2d limit_prox = {min(lim_prox_n * abs(dot(c2, ex)),
                              lim_prox_s * abs(dot(c1, ex))),
                              min(lim_prox_n * abs(dot(c2, ey)),
                              lim_prox_s * abs(dot(c1, ey)))};
          double nf = cd.siku.phys_consts["nudge_fac"];
          //CSD if they are both CONTROL elements don't let them alter the nudging coefficient
          //CSD this is getting really convoluted. I really want to accumulate the
          //CSD total stress on the control element face and set the criteria based on this. but
          //CSD for now replace the vnudge estimate if it is non zero and smaller than the current estimate
          if (cd.e1.flag & Element::F_CONTROLLED && ~cd.e2.flag & Element::F_CONTROLLED){
              cd.e1.Vnudge = {min(nf * limit_prox.x,cd.e1.Vnudge.x),
                              min(nf * limit_prox.y,cd.e1.Vnudge.y)};
          }
          if (cd.e2.flag & Element::F_CONTROLLED && ~cd.e1.flag & Element::F_CONTROLLED) {
              cd.e2.Vnudge = {min(nf * limit_prox.x,cd.e2.Vnudge.x),
                              min(nf * limit_prox.y,cd.e2.Vnudge.y)};
          }
      }
      // CSD don't allow interaction if they are both CONTROL elements,
      //     otherwise express interaction
      if (~cd.e1.flag & Element::F_CONTROLLED && ~cd.e2.flag & Element::F_CONTROLLED)
      {
         if_.rf1 = cd.r1 * cd.siku.planet.R;
         if_.rf2 = (cd.r1 - cd.r12) * cd.siku.planet.R;
         if_.F1 = F;
         if_.F2 = - F;
         if_.couple1 = vt;
         if_.couple2 = -vt;
      }
    }

  return if_;
}

// -----------------------------------------------------------------------

InterForces _test_springs( ContactData& cd )
{
  InterForces if_{};

  // physical rigidity of ice (from python scenario)
  double K = _rigidity( cd );
  // TODO: discuss viscosity
  double area = pow( (cd.c.init_wid * cd.siku.planet.R * 0.5 ), 2 ) * M_PI;

  // calculating forces and torques
  vec2d p1 = cd.c.p1; // same as c.p2
  vec2d p2 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p3 ) ); // || c.p4
  vec2d def = p2 - p1;

  vec2d F = (def * cd.siku.planet.R) * K * (cd.c.init_wid  * cd.siku.planet.R)
              * cd.c.durability
              - area * cd.siku.phys_consts["etha"] * cd.va12;  // viscous

  // memorizing deformation
  cd.d1 = cd.d2 = abs( def ) * cd.siku.planet.R;

  if_.rf1 = p1 * cd.siku.planet.R;
  if_.rf2 = (p2 - cd.r12) * cd.siku.planet.R;
  if_.F1  = F;
  if_.F2  = -F;
  //if_.couple1 = if_.couple2 = 0.; by default

  return if_;
}

// -----------------------------------------------------------------------

inline bool _teta(ContactData& cd, double ss, double sn)
{
  double Ss = cd.siku.phys_consts["sigma_s"],
         Sc = cd.siku.phys_consts["sigma_c"],
         St = cd.siku.phys_consts["sigma_t"],
         tm = cd.siku.phys_consts["tan_mu"];

  return ((sn >= 0 && sn < St) && (ss > -Ss && ss < Ss))
      || ((sn > -Sc && sn < 0) && (abs(ss) < Ss - sn*tm));
}

inline void _hop_rig(ContactData& cd, double& hn, double& hs)
{
  // ice thickness at largest (main) layer
  double h1 = cd.e1.h_main, h2 = cd.e2.h_main;

  // elasticity of elements
  double E1 = cd.siku.ms[cd.e1.imat].E, E2 = cd.siku.ms[cd.e2.imat].E;
  double nu1 = cd.siku.ms[cd.e1.imat].nu, nu2 = cd.siku.ms[cd.e2.imat].nu;

  hn = (h1*E1 + h2*E2) * 0.5;
  hs = (h1*E1/( 2.*(1. + nu1)) + h2*E2/( 2.*(1. + nu2) )) * 0.5;
  return;
}

InterForces _hopkins_frankenstein( ContactData& cd )
{
  InterForces if_{}; // should be filled with zeroes

  // original contact points considering current shift of elements (SI)
  vec2d p1 = cd.c.p1 * cd.siku.planet.R,
        p2 = cd.c.p2 * cd.siku.planet.R,
        p3 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p3 ) )
          * cd.siku.planet.R,
        p4 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p4 ) )
          * cd.siku.planet.R;

  vec2d dr1 = p4 - p1,
        dr2 = p3 - p2;

  // tangential vector to the middle line of joint and normal vector
  vec2d tau = (p1 + 0.5*dr1) - (p2 + 0.5*dr2);
  double L = abs(tau);
  tau /= L;
  vec2d norm = rot_90_ccw(tau);

  vec2d r0 = p1 + 0.5*dr1;
  double r01 = cross(r0, norm),
         r02 = cross(r0 - cd.r12*cd.siku.planet.R, norm);

  // elasticities...
  double hn, hs;
  _hop_rig( cd, hn, hs );
  hn /= L, hs /= L;

  // tangential and normal displacements
  double Ds = 0.5 * dot( (dr1+dr2), tau );
  double Dn1 = dot( dr1, norm ), Dn2 = dot( dr2, norm ), aaa = Dn2-Dn1;

  // segments length for integration
  double dDz = 1./cd.siku.phys_consts["n_integ_segments"],
         Dz0 =  0.5 * dDz;

  // accumulators and temporals for integral
  double N1{}, N2{};
  double Dz, Sn, Dn, Ss = Ds*hs;
  vec2d Fn{}, Fs{}, Fni{}; // Fsi - constant;

  double durability = 1.; // to accumulate destruction.

//  norm *= dDz*hn; // factored out of integral (norm no longer used as unit vec)

  // kinda integration
  for(double i=0; i<cd.siku.phys_consts["n_integ_segments"]; ++i)
    {
      Dz = Dz0 + i*dDz;
      Dn = Dn1 + Dz*aaa;
      Sn = hn*Dn;

      if(!_teta(cd, Ss, Sn))
        {
          durability -= dDz;
          continue;
        }

      Fni = Sn*dDz*norm;
      Fn += Fni;
      // Fsi = constant

      N1 += (r01 + Dz*L) * Fni;
      N2 += (r02 + Dz*L) * -Fni;
    }

  // tangential force
  //Fsi = Ss * tau
  Fs = Ss * tau * durability;
  N1 += cross( r0, Fs );
  N2 += cross( Fs, r0 - cd.r12*cd.siku.planet.R );//reverse order instead of '-'

  // applying (L was factored out)
  if_.F1 = (Fs + Fn) * L;
  if_.F2 = -if_.F1;
  if_.couple1 = N1 * L;
  if_.couple2 = N2 * L;

  // --- additional viscosity ---

  // viscous force
  double area = L * L * M_PI * 0.25;
  vec2d C = (p1 + p2 + p3 + p4) * 0.25;
  vec2d Fv = - area * cd.siku.phys_consts["hop_visc"] * cd.va12;
  double vt = (cd.e2.W.z - cd.e1.W.z) * cd.siku.phys_consts["hop_visc"]
              * pow( area, 2 ) / (6. * M_PI) ;

  if_.F1 += Fv;
  if_.F2 += - Fv;
  if_.couple1 += cross( C, Fv ) + vt;
  if_.couple2 += cross( Fv, C - cd.r12*cd.siku.planet.R ) - vt;

  // --- adjusting force according to durability ---
  if_.F1 *= cd.c.durability;
  if_.F2 *= cd.c.durability;
  if_.couple1 *= cd.c.durability;
  if_.couple2 *= cd.c.durability;
  //if_.rf1 = if_.rf2 = {} by default

  // durability passing
  cd.d1 = durability;

  return if_;
}

// -----------------------------------------------------------------------

InterForces _distributed_springs( ContactData& cd )
{
  InterForces if_{};

  // physical rigidity of ice (from python scenario)
  double K = _rigidity( cd );

  vec3d tv1, tv2; // just some temporals

  // original contact points considering current shift of elements (SI)
  vec2d p1 = cd.c.p1 * cd.siku.planet.R,
        p2 = cd.c.p2 * cd.siku.planet.R,
        p3 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p3 ) )
          * cd.siku.planet.R,
        p4 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p4 ) )
          * cd.siku.planet.R,
        p42 = cd.c.p4 * cd.siku.planet.R,
        p32 = cd.c.p3 * cd.siku.planet.R;
  
  // some additional variables to avoid unnecessary functions` calls
  double hardness = K * (cd.c.init_wid * cd.siku.planet.R),// * cd.c.durability,
         rotablty = hardness * 1./12.,
         area = pow( (cd.c.init_wid * cd.siku.planet.R * 0.5 ), 2 ) * M_PI;
      // TODO: implement some kind of form factor for proper 'area' calculation

  cd.dr1 = p4 - p1,
  cd.dr2 = p3 - p2;
  cd.ed1 = - unit((p1 - p2) * cross(-p2,p1-p2));
  cd.ed2 = - unit((p42 - p32) * cross(-p32,p42-p32));
  // memorizing deformations
  cd.d1 = abs( cd.dr1 );
  cd.d2 = abs( cd.dr2 );

  // The Force itself
  // TODO: recombine viscous force with respective method inside 'collision'
  // CSD let's try modifying this to allow for spring strength to be different in
  // CSD the shear orientation using the poisson ratio
  
  // vec2d F = hardness * (cd.dr1 + cd.dr2) * 0.5;   // elastic
     vec2d stretch = (cd.dr1 + cd.dr2) * 0.5;
  // vec2d const c1 = unit(cd.ed1);
  //   vec2d const c2 = unit(rot_90_ccw(cd.ed1));
  //   vec2d ex { 1., 0. }, ey { 0., 1. };
  //   // CSD convert strains to force along and orthogonal to contact face
  //   // CSD really we should do this with a matrix operation and the matrix transpose.
  //   vec2d const Fp = {hardness * dot(stretch, c1),
  //                     hardness * dot(stretch, c2)};
  //  //                 / (2.0 * (1.0 + cd.siku.ms[cd.e1.imat].nu)) };
  //  // CSD get force components in x-y plane, oblique method of applying transpose of matrix
  //   vec2d F = {Fp.x * dot(c1, ex) + Fp.y * dot(c2, ex),
  //              Fp.x * dot(c1, ey) + Fp.y * dot(c2,ey)};
     vec2d F = hardness * stretch;
     F-= area * cd.siku.phys_consts["etha"] * cd.va12;  // viscous

  // viscous torque // UNDONE: correct multiplier yet unknown and has
  // no correlation with 'collision' force
  double vt =  (cd.e2.W.z - cd.e1.W.z) * cd.siku.phys_consts["etha"]
               * pow( area, 2 ) / (6. * M_PI) ;

  if_.F1 = F;
  if_.F2 = -F;
  if_.rf1 = (p1 + p2) * 0.5;
  if_.rf2 = (p3 + p4) * 0.5  - cd.r12 * cd.siku.planet.R;
  if_.couple1 = rotablty * cross( p1 - p2, cd.dr1 - cd.dr2 ) + vt;
  if_.couple2 = -if_.couple1;
//// IMPROVE: clean dis mess
//      rotablty * cross( p1 - p2, (p4 + p2) - (p1 + p3) ) // same as above

  return if_;
}

// -----------------------------------------------------------------------

InterForces _wilch_like( ContactData& cd )
{
  InterForces if_{};

  // physical rigidity of ice (from python scenario)
  double K = _rigidity( cd );
  double Kg = _shear_mod( cd );

  vec3d tv1, tv2; // just some temporals

  // original contact points considering current shift of elements (SI)
  vec2d p1 = cd.c.p1 * cd.siku.planet.R,
        p2 = cd.c.p2 * cd.siku.planet.R,
        p3 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p3 ) )
          * cd.siku.planet.R,
        p4 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p4 ) )
          * cd.siku.planet.R,
        p42 = cd.c.p4 * cd.siku.planet.R,
        p32 = cd.c.p3 * cd.siku.planet.R;

  // some additional variables to avoid unnecessary functions` calls
  double hardness = K * (cd.c.init_wid * cd.siku.planet.R),// * cd.c.durability,
         hardnessG = Kg * (cd.c.init_wid * cd.siku.planet.R),
         rotablty = hardness * 1./12.,
         area = pow( (cd.c.init_wid * cd.siku.planet.R * 0.5 ), 2 ) * M_PI;
      // TODO: implement some kind of form factor for proper 'area' calculation

  cd.dr1 = p4 - p1,
  cd.dr2 = p3 - p2;
  cd.ed1 = - unit((p1 - p2) * cross(-p2,p1-p2));
  cd.ed2 = - unit((p42 - p32) * cross(-p32,p42-p32));
  // memorizing deformations
  cd.d1 = abs( cd.dr1 );
  cd.d2 = abs( cd.dr2 );

  // The Force itself
  // TODO: recombine viscous force with respective method inside 'collision'
  // CSD let's try modifying this to allow for spring strength to be different in
  // CSD the shear orientation using the poisson ratio

  // vec2d F = hardness * (cd.dr1 + cd.dr2) * 0.5;   // elastic
     vec2d stretch = (cd.dr1 + cd.dr2) * 0.5;
     vec2d const c1 = unit(cd.ed1);  // tangent to face
     vec2d const c2 = unit(rot_90_ccw(cd.ed1));  // normal to face inward pointing
     vec2d ex { 1., 0. }, ey { 0., 1. };
      // CSD convert strains to force along and orthogonal to contact face
      //
     vec2d const Fp = {hardnessG * dot(stretch, c1),
                       hardness * dot(stretch, c2)};
     //  //                 / (2.0 * (1.0 + cd.siku.ms[cd.e1.imat].nu)) };
     // CSD get force components in x-y plane, oblique method of applying transpose of matrix
     vec2d F = {Fp.x * dot(c1, ex) + Fp.y * dot(c2, ex),
                Fp.x * dot(c1, ey) + Fp.y * dot(c2, ey)};

     // CSD save stress along and normal to element face
     double h_avg = 0.5 * (cd.e1.h_main + cd.e2.h_main);
     cd.c.Sn = K / h_avg * dot(stretch, c2);
     cd.c.Ss = Kg / h_avg * dot(stretch, c1);


     F-= area * cd.siku.phys_consts["etha"] * cd.va12;  // viscous
     /*
     if (cd.c.type==COMPRESSIVE_FAILURE &&
         (cd.c.failure_status == SHEAR ||
          cd.c.failure_status == TENSION))
     {
         double sig_max_c   = cd.siku.ms[0].layers[0].sigma_c;
         cd.c.shr_weakening = 0.1 + 0.9 * cd.c.Sn/sig_max_c;
     }
     else if
             (cd.c.type==COMPRESSIVE_FAILURE &&
              cd.c.failure_status == COMPRESSION )
     {
         cd.c.shr_weakening = 0.1;
         cd.c.nrm_weakening = 0.1;
     }  */


  // viscous torque // UNDONE: correct multiplier yet unknown and has
  // no correlation with 'collision' force
  double vt =  (cd.e2.W.z - cd.e1.W.z) * cd.siku.phys_consts["etha"]
               * pow( area, 2 ) / (6. * M_PI) ;

  if_.F1 = F;
  if_.F2 = -F;
  if_.rf1 = (p1 + p2) * 0.5;
  if_.rf2 = (p3 + p4) * 0.5  - cd.r12 * cd.siku.planet.R;
  if_.couple1 = rotablty * cross( p1 - p2, cd.dr1 - cd.dr2 ) + vt;
  if_.couple2 = -if_.couple1;
//// IMPROVE: clean dis mess
//      rotablty * cross( p1 - p2, (p4 + p2) - (p1 + p3) ) // same as above

  return if_;
}

// -----------------------------------------------------------------------

InterForces _edge_orient( ContactData& cd )
{
  InterForces if_{};

  // original contact points considering current shift of elements (SI)
  // CSD um...no. The p[1234] variables here are determined in dist_freeze
  // to be the midpoints of the springs between the vertices, not the vertex
  // locations. So below would give how the two estimates of the midpoint location
  // seperate in time as e2_to_e1 evolves
  /* vec2d p1 = cd.c.p1 * cd.siku.planet.R,
        p2 = cd.c.p2 * cd.siku.planet.R,
        p3 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p3 ) )
          * cd.siku.planet.R,
        p4 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p4 ) )
          * cd.siku.planet.R,
        p42 = cd.c.p4 * cd.siku.planet.R,
        p32 = cd.c.p3 * cd.siku.planet.R; */
  // CSD check to see if e2_to_e1 maintains unit vector
  /* vec3d P4chk = cd.e2_to_e1 *  cd.e2.P[cd.c.v22];
  vec3d P3chk = cd.e2_to_e1 *  cd.e2.P[cd.c.v21];
  vec2d P4chk2d = vec3_to_vec2(P4chk);
  vec2d P3chk2d = vec3_to_vec2(P3chk);

  double dist3 = glm::length(P4chk-P3chk);
  double dist2v = abs(P4chk2d-P3chk2d);  */

  vec2d p1 = vec3_to_vec2(cd.e1.P[cd.c.v11]) * cd.siku.planet.R,
        p4 = vec3_to_vec2(cd.e2_to_e1 * cd.e2.P[cd.c.v22]) * cd.siku.planet.R;

  vec2d p2 = vec3_to_vec2(cd.e1.P[cd.c.v12]) * cd.siku.planet.R,
        p3 = vec3_to_vec2(cd.e2_to_e1 * cd.e2.P[cd.c.v21]) * cd.siku.planet.R;

  vec2d p42 = vec3_to_vec2(cd.e2.P[cd.c.v22]) * cd.siku.planet.R,
        p32 = vec3_to_vec2(cd.e2.P[cd.c.v21]) * cd.siku.planet.R;

  cd.dr1 = p4 - p1,
  cd.dr2 = p3 - p2;
  //CSD in some instances it seems that dr1 and dr2 can just represent the
  // error in the coordinate projections at the initial step, and this error
  // can be as large as ~1e-9. So let's round the dr1 and dr2 vectors at the
  // 8th decimal place...with a small enough timestep, this jostling doesn't
  //amount to anything but a background vibration.
  /*cd.dr1.x = round(cd.dr1.x*1e8)/1e8;
  cd.dr1.y = round(cd.dr1.y*1e8)/1e8;
  cd.dr2.x = round(cd.dr2.x*1e8)/1e8;
  cd.dr2.y = round(cd.dr2.y*1e8)/1e8;*/
  // cd.ed1 = unit((p1 - p2) * cross(-p2,p1-p2)/abs(cross(-p2,p1-p2)));
  // ed1 and ed2 become more accurate as the vertices seperate oddly enough, because
  // when the vertices are really close they just reflect roundoff error...
  // (CSD ..see roundoff fix above).
  //ed2 is not used for anything.
  cd.ed1 = - unit(p1 - p2) * copysign(1.0, cross(-p2, p1-p2));  // CSD neg. if p2 is CW of p1
  cd.ed2 = - unit(p42 - p32) * copysign(1.0, cross(-p32, p42-p32));
  // memorizing deformations
  cd.d1 = abs( cd.dr1 );
  cd.d2 = abs( cd.dr2 );

  // vec2d F = hardness * (cd.dr1 + cd.dr2) * 0.5;   // elastic
     vec2d stretch = (cd.dr1 + cd.dr2) * 0.5;
     vec2d stretch_diff = (cd.dr1 - cd.dr2) * 0.5;
     vec2d c1 = unit(orthog(-cd.r1));   // directed tangential to COM
     vec2d c2 = unit(-cd.r1);   // directed at COM
     vec2d const f1 = unit(cd.ed1);  // tangent to face
     vec2d const f2 = unit(rot_90_ccw(cd.ed1));  // normal to face inward pointin
     vec2d ex { 1., 0. }, ey { 0., 1. };
     //CSD when considering shear failure under compression, we need to transition
     //CSD the behavior of the contact from an orientation relative to the element
     //CSD to a behavior relative to the contact faces so
     c1 = c1 * double(cd.c.type == ContType::JOINT)
        + f1 * double(cd.c.type == ContType::SHEAR_CMP_FAILURE);
     c2 = c2 * double(cd.c.type == ContType::JOINT)
        + f2 * double(cd.c.type == ContType::SHEAR_CMP_FAILURE);
     //CSD if at failure weaken the spring strengths, potentially
     //CSD using Sn estimate from previous time step.
     /*if (cd.c.type==ContType::COMPRESSIVE_FAILURE &&
      (cd.c.failure_status == SHEAR ||
       cd.c.failure_status == TENSION))
  { 
      cd.c.shr_weakening = 0.01 + 0.09 * cd.c.Sn/sig_max_c * (1.0-abs(dot(c1,f2)));
      cd.c.nrm_weakening = abs(dot(c2,f1));
      //cd.c.shr_weakening = 0.1;
  }
  else if
          (cd.c.type==ContType::COMPRESSIVE_FAILURE &&
           cd.c.failure_status == COMPRESSION )
  {
      cd.c.shr_weakening = 0.01 + 0.09 * cd.c.Sn/sig_max_c * (1.0-abs(dot(c1,f2)));
      cd.c.nrm_weakening = 0.1;
  }  */

  // physical rigidity of ice (from python scenario)
  double K = _rigidity( cd );
  double Kg = _shear_mod( cd );

  //vec2d Fp;
  //vec3d tv1, tv2; // just some temporals

  // original contact points considering current shift of elements (SI)
  /*vec2d p1 = cd.c.p1 * cd.siku.planet.R,
        p2 = cd.c.p2 * cd.siku.planet.R,
        p3 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p3 ) )
          * cd.siku.planet.R,
        p4 = vec3_to_vec2( cd.e2_to_e1 * vec2_to_vec3( cd.c.p4 ) )
          * cd.siku.planet.R,
        p42 = cd.c.p4 * cd.siku.planet.R,
        p32 = cd.c.p3 * cd.siku.planet.R;
  */
  // some additional variables to avoid unnecessary functions` calls
  double hardness = K * (cd.c.init_wid * cd.siku.planet.R),// * cd.c.durability,
     //CSD If the joint fails in shear, while the contacting elements
    //CSD overlap, reduce the shear strength (hardnessG) to zero
         hardnessG = Kg * (cd.c.init_wid * cd.siku.planet.R)
                        * (cd.c.type != ContType::SHEAR_CMP_FAILURE),
          //CSD I do not understand the 1/12th factor here.
         //rotablty = hardness * 1./12.,
         //CSD as an experiment lets limit the capability of the elements to torque
         rotablty = 1.,
         area = pow( (cd.c.init_wid * cd.siku.planet.R * 0.5 ), 2 ) * M_PI;
  double inv_sec = 1 / (cd.c.init_wid * cd.siku.planet.R);
      // TODO: implement some kind of form factor for proper 'area' calculation
  /*cd.dr1 = p4 - p1,
  cd.dr2 = p3 - p2;
  // cd.ed1 = unit((p1 - p2) * cross(-p2,p1-p2)/abs(cross(-p2,p1-p2)));
  cd.ed1 = - unit((p1 - p2) *cross(-p2, p1-p2));  // CSD neg. if p2 is CW of p1
  cd.ed2 = - unit((p42 - p32) * cross(-p32,p42-p32));
  // memorizing deformations
  cd.d1 = abs( cd.dr1 );
  cd.d2 = abs( cd.dr2 );

  // The Force itself
  // TODO: recombine viscous force with respective method inside 'collision'
  // CSD let's try modifying this to allow for spring strength to be different in
  // CSD the shear orientation using the poisson ratio

  // vec2d F = hardness * (cd.dr1 + cd.dr2) * 0.5;   // elastic
     vec2d stretch = (cd.dr1 + cd.dr2) * 0.5;
     vec2d const c1 = unit(orthog(cd.r12));   // directed tangential to COM
     vec2d const c2 = unit(cd.r12);   // directed at COM
     vec2d const f1 = unit(cd.ed1);  // tangent to face
     vec2d const f2 = unit(rot_90_ccw(cd.ed1));  // normal to face inward pointin
     vec2d ex { 1., 0. }, ey { 0., 1. };
  */
     // The Force itself
     // TODO: recombine viscous force with respective method inside 'collision'
     // CSD let's try modifying this to allow for spring strength to be different in
     // CSD the shear orientation using the poisson ratio

     // CSD convert strains to force along and orthogonal to centerline between elements
     /* if (cd.c.type==COMPRESSIVE_FAILURE) {
         //CSD if in a fail state weaken tangential component of contact force
         //CSD amount of elasticity depends on 'weakening' parameter in hardnessG calc
         //CSD sliding resistance is proportional to the force normal to the face
         double sig_max_t = -cd.siku.ms[0].layers[0].sigma_t,
                coef_fric = cd.siku.ms[0].mu;
         double Fs_slide = 0.1 *coef_fric * (cd.c.Sn - sig_max_t * 1000) / inv_sec;
         Fp = {hardnessG * dot(stretch, c1) + Fs_slide,
               hardness * dot(stretch, c2)};
     }
     else  {  */
     vec2d Fp = {hardnessG * dot(stretch, c1),
                 hardness * dot(stretch, c2)};
     // CSD for calculating the couple use a consistent force, by getting the force
     // at each vertex
     vec2d Fp1 = {hardnessG * dot(cd.dr1, c1),
                  hardness * dot(cd.dr1, c2)};
     vec2d Fp2 = {hardnessG * dot(cd.dr2, c1),
                  hardness * dot(cd.dr2, c2)};
     //CSD try wilchinsky-like orientation everywhere
     /* vec2d Fp1 = {hardnessG * dot(cd.dr1, f1),
                  hardness * dot(cd.dr1, f2)};
     vec2d Fp2 = {hardnessG * dot(cd.dr2, f1),
                  hardness * dot(cd.dr2, f2)}; */
     vec2d Fpp1 = Fp1-Fp;
     vec2d Fpp2 = Fp2-Fp;
     vec2d drp1 = cd.dr1-stretch;
     vec2d drp2 = cd.dr2-stretch;
     //CSD calculate the internal energy of the contact as well here
     //..right now we are calculating instantaneous..but we want time averaged
     cd.c.W_strain = 0.5 * (abs((Fp.x) * (dot(stretch, c1))) +
                            abs((Fp.y) * (dot(stretch, c2))));
     cd.c.W_couple = (abs((Fpp1.x) * (dot(drp1, c1))) +
                            abs((Fpp1.y) * (dot(drp1, c2))))
                    +(abs((Fpp2.x) * (dot(drp2, c1))) +
                            abs((Fpp2.y) * (dot(drp2, c2))));

     // }
     //  //                 / (2.0 * (1.0 + cd.siku.ms[cd.e1.imat].nu)) };
     // CSD get force components in x-y plane, indirect method of applying transpose of matrix
     vec2d F = {Fp.x * dot(c1, ex) + Fp.y * dot(c2, ex),
                Fp.x * dot(c1, ey) + Fp.y * dot(c2, ey)};

     vec2d F1 = {Fp1.x * dot(c1, ex) + Fp1.y * dot(c2, ex),
                Fp1.x * dot(c1, ey) + Fp1.y * dot(c2, ey)};
     vec2d F2 = {Fp2.x * dot(c1, ex) + Fp2.y * dot(c2, ex),
                Fp2.x * dot(c1, ey) + Fp2.y * dot(c2, ey)};
     // CSD save stress along and normal to element face
     // double h_avg = 0.5 * (cd.e1.h_main + cd.e2.h_main);
     //cd.c.Sn = inv_sec * dot(F, f2);
     //cd.c.Ss = inv_sec * dot(F, f1);
     //CSD Try orienting normal and tangential to centerline connecting elements
     cd.c.Sn = inv_sec * dot(F, c2);
     cd.c.Ss = inv_sec * dot(F, c1);

     cd.c.F_elj = F;
     /* CSD Calculate factors that measure how far the stress at the contact
        is from a specified stress state for control elements.  For now
        let me just specify the state as being 1/2 the maximum normal \
        stress and not control the shear stress yet*/
     if (cd.e1.flag & Element::F_CONTROLLED || cd.e2.flag & Element::F_CONTROLLED) {
         double lim_prox_n, lim_prox_s;
         double brf = cd.siku.phys_consts["bnd_resist_fac"];
         if (cd.c.Sn>=0) {
            lim_prox_n = min(
                             max(cd.siku.ms[0].layers[0].sigma_c *1000-cd.c.Sn*brf,0.0)
                             /(cd.siku.ms[0].layers[0].sigma_c *1000)
                             ,1.0);
         }
         else {
            lim_prox_n = min(
                             max(cd.siku.ms[0].layers[0].sigma_t*1000+cd.c.Sn*brf,0.0)
                             /(cd.siku.ms[0].layers[0].sigma_t *1000)
                             ,1.0);
          }
         lim_prox_s = min(
                          max(cd.siku.ms[0].mu*cd.siku.ms[0].layers[0].sigma_t*1000
                          - abs(cd.c.Ss),0.0)
                          /(cd.siku.ms[0].mu*cd.siku.ms[0].layers[0].sigma_t *1000)
                          ,1.0);
       //We want to shut off the resistance to motion if either the normal or shear
       // stresses exceed the criteria we set.
        vec2d limit_prox = {min(lim_prox_n * abs(dot(f2, ex)),
                            lim_prox_s * abs(dot(f1, ex))),
                            min(lim_prox_n * abs(dot(f2, ey)),
                            lim_prox_s * abs(dot(f1, ey)))};
        double nf = cd.siku.phys_consts["nudge_fac"];
        if (cd.e1.flag & Element::F_CONTROLLED){
            cd.e1.Vnudge = {nf * limit_prox.x, nf * limit_prox.y};
           //  cd.e1.Vnudge = {0., 0.};     //check
        }
        if (cd.e2.flag & Element::F_CONTROLLED) {
            cd.e2.Vnudge = {nf * limit_prox.x, nf * limit_prox.y};
         //   cd.e1.Vnudge = {0., 0.};  //check
        }
     }

     if (cd.c.type == ContType::SHEAR_CMP_FAILURE) {
         cd.c.va12_cb.push_back(cd.va12);
         vec2d va12_avg={0.,0.};
         vec2d va12_avg_chk, va12_i_chk;
         for (unsigned int i=0; i<cd.c.va12_cb.size(); i++) {
             va12_avg += cd.c.va12_cb[i];
             va12_i_chk = cd.c.va12_cb[i];
             va12_avg_chk = va12_avg;
         }
         va12_avg /=cd.c.va12_cb.size();

         double f_v_negate = abs( dot(va12_avg, c1) / cd.siku.time.get_dt()
                                * min (cd.e1.m, cd.e2.m));
         // If the blocks are in relative motion and there is a compressive force on the face, apply a force to inhibit
         // the motion up to but not exceeding the coulomb friction
         vec2d F_fric_adj{0.,0.};
         double f_fric_max = max(cd.siku.phys_consts["fric_dissp"] * dot(F, c2),0.);
         //if (cd.siku.phys_consts["fric_dissp"] > 0 & abs(dot(F,c1)) > abs( f_fric_max ) & abs( dot( cd.va12,c1 )) > 0)
         if (cd.siku.phys_consts["fric_dissp"] > 0 & abs(dot(cd.va12,c1)) > 0)
         {
             F_fric_adj = copysign(1.0, dot(cd.va12, c1)) * min(f_fric_max, f_v_negate) * c1;
         }

         F -= F_fric_adj;

         cd.c.Ss += inv_sec * dot(F_fric_adj, c1);
         cd.c.W_fric = abs(F_fric_adj) * abs(dot(va12_avg, c1)) * cd.siku.time.get_dts();
     }
     //CSD this form of viscoscity can introduce numerical instabilities
     //CSD if set too large
     F-= area * cd.siku.phys_consts["etha"] * cd.va12;  // viscous
     //CSD Adjust the spring weakening parameters proportional to
     //CSD the normal stress on the joint.  In this formulation
     //CSD I consider the orientation of the joint relative to the
     //CSD normal to the COM to determine how much the shear spring and
     //CSD how much the normal spring strengths are weakened.
     //CSD Calculate a weakening coefficient as soon as durability drops below 1
     //CSD but don't use that coeffcient until a COMPRESSIVE_FAILURE state is reached
     //CSD this is a work-around to deal with the timing of when the joint destruction
     //CSD occurs and when the weakning parameters are calculated. ...alternatively
     //CSD this
     /* if (cd.c.durability < 1.0 &&
         (cd.c.failure_status == SHEAR ||
          cd.c.failure_status == TENSION))
     {
         double sig_max_c   = cd.siku.ms[0].layers[0].sigma_c;
         /* cd.c.shr_weakening = 0.1 + 0.9 * cd.c.Sn/sig_max_c * 1.0-abs(dot(c1,f2));
         cd.c.nrm_weakening = abs(dot(c2,f1));
         cd.c.shr_weakening = 0.1;
     }
     else if
             (cd.c.durability < 1.0 &&
              cd.c.failure_status == COMPRESSION )
     {
         double sig_max_c   = cd.siku.ms[0].layers[0].sigma_c;
         cd.c.shr_weakening = 0.1;
         cd.c.nrm_weakening = 0.1;
     } */
  // viscous torque // UNDONE: correct multiplier yet unknown and has
  // no correlation with 'collision' force
  double vt =  (cd.e2.W.z - cd.e1.W.z) * cd.siku.phys_consts["etha"]
               * pow( area, 2 ) / (6. * M_PI) ;
  // CSD note here if_.F2 is the force on element 2, while F2 in this routine
  //     is the force in the e1 frame at the second contact vertices.
  if_.F1 = F;
  if_.F2 = -F;
  if_.rf1 = (p1 + p2) * 0.5;
  if_.rf2 = (p3 + p4) * 0.5  - cd.r12 * cd.siku.planet.R;
  //CSD I don't like the 1/12*hardness estimate of the torque, lets be explicit and use the forces
  //CSD at the vertices rather than the stretch
  // if_.couple1 = rotablty * cross( p1 - p2, cd.dr1 - cd.dr2 ) + vt;
  if_.couple1 = rotablty * 0.5 * cross( p1 - p2, F1 - F2 ) + vt;
  if_.couple2 = -if_.couple1;
//// IMPROVE: clean dis mess
//      rotablty * cross( p1 - p2, (p4 + p2) - (p1 + p3) ) // same as above

  return if_;
}
