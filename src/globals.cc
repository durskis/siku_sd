/*
 * Siku: Discrete element method sea-ice model: description.hh
 *
 * Copyright (C) UAF
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

/*!

 \file globals.cc

 \brief Initialization and some processing for Globals structure

 */

#include "globals.hh"
#include "sikupy.hh"
#include "coordinates.hh"

#include "fstream"

using namespace Coordinates;
#include <fstream>
// --------------------------------------------------------------------------

Globals::Globals()
{
  // default empty-string first members
  cons.push_back( std::string( "" ) );
  mons.push_back( std::string( "" ) );
}

// --------------------------------------------------------------------------

void Globals::post_init(Sikupy& sikupy)
{
  wind.init( wind.FIELD_SOURCE_TYPE );
  water.init( water.FIELD_SOURCE_TYPE );

  for( size_t i =0; i < es.size(); ++i )
    {
      // setting new elements id
      es[i].id = i;

      vec3d temp;

      if( ! (es[i].flag & Element::F_PROCESSED) )  //only for new elements
        {
          // setting default (loaded from .py) velocity and rotation
          double lat, lon;
          sph_by_quat ( es[i].q, &lat, &lon );

          // for new elements velocity must be inputed in East-North terms
          temp = glob_to_loc ( es[i].q, geo_to_cart_surf_velo(
              lat, lon, es[i].V.x, es[i].V.y ) );

          // CSD es.V is used on the first time step but on the second time step
          // it is calculated based on W. 'temp' determines W before the first time step
          //, but is a redefinition
          // of es.V, consequently the es.V seen by the model on the second and all
          // other time steps is something like {-es.V.y, es.V.x, 0} by the original
          // definition of es.V. es.V needs to be adjusted here for consistency.
          // es.V as read in appears as a typical choice of coordinates {e-vel, n-vel,0}.
          // temp on the other hand is {s-vel, e-vel, 0} oddly enough.  But we
          // should leave this for consistency with everything else in the model.
          // This is just damn sloppy.
          // es[i].V = vec3d(temp.x, temp.y, es[i].V.z);

          // CSD - TEST - let's say that the velocity read in is in the
          // local 2D coordinates rather than e-vel, n-vel.
          es[i].V = vec3d(-es[i].V.y, es[i].V.x, es[i].V.z);
          // removing duplicated vertices
          auto& P = es[i].P;
          size_t ss = P.size();

          // improve: points equality test
          for( size_t j = 0; j < ss; )
//            if( abs( P[ j ] - P[ (j+1)%ss ] ) < 1e-14 )
            if( P[ j ] == P[ (j+1)%ss ] )
              P.erase( P.begin() + (j+1)%(ss--) );
            else
              ++j;
        }
      else
        {
          temp = es[i].V;
        }

      es[i].W = vec3d( -temp.y * planet.R_rec, temp.x * planet.R_rec,
                       es[i].V.z );

    }

  if( mark_borders )
    {
      cout<<"Marking borders with points\n";
      cout<< bord_file << endl;

      // reading borders
      std::ifstream in( bord_file.c_str() );
      std::vector<vec3d> points; // global (x,y,z)
      double d1, d2;
      vec3d point;

      cout<< "Started while loop" << endl;

      while( !in.eof() )
      {
        in >> d1 >> d2;
        //        cout << d1 << " " << d2 << endl;

        point =
            sph_to_cart( 1., M_PI / 2. - deg_to_rad( d2 ), deg_to_rad( d1 ) );

        points.push_back( point );
      }

      in.close();

      cout<<"HERE" << endl;

      // marking borders by border points (2d approximation)
      size_t size = points.size();
      for( auto& e : es )
        {
          if( e.flag & Element::F_PROCESSED )  // only for new elements
            continue;

          for( size_t i = 0; i < size; ++i )
            {
              if( e.contains( points[i] ) )
                {
                  std::swap( points[i], points[--size] );
                  points.pop_back();

                  e.flag = ( e.flag &~ Element::F_MOVE_FLAG ) //clear move flag
                      | Element::F_STATIC; //mark as static
                }
            }
        }
      cout<<"Done\n\n";
    }

  // freezing start ice blocks
  //ConDet.freeze( *this );
}

// --------------------------------------------------------------------------

void Globals::add_monit( std::string& mon, Element& e )
{
  // search for matching registered monitor name
  for( size_t i =0; i < mons.size(); ++i )
    {
      if( ! mons[i].compare( mon ) ) // 0 means equal
        {
          //set found monitor name index
          e.mon_ind = i;
          return;
        }
    }

  // if name was not found - add new one
  mons.push_back( mon );
  e.mon_ind = mons.size() - 1;
  return;
}

// --------------------------------------------------------------------------

void Globals::add_contr( std::string& con, Element& e )
{
  // YET UNUSED
  /*
   * TODO: Add control functions all around the program.
   */
}

