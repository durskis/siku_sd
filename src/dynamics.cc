/*!

 \file dynamics.cc

 \brief Implementation of dynamics function. The dynamics is update
 on velocities and angular velocity of each ice element depending on
 the forces and torques applied (and previously calculated). All
 velocities are coded in a single 3D angular velocity vector set in
 body frame.

 */

#include "dynamics.hh"
#include "coordinates.hh"

//#include "planet.hh"

///////////
#include <iostream>

void
dynamics ( Globals& siku, const double dt )
{
  double time_diff=dt*siku.time.get_n();  // csd time since start in seconds
  for ( auto & e : siku.es )
    {
//      if( e.flag & Element::F_ERRORED ) continue; // TODO: change or remove

      if ( e.flag & Element::F_STATIC || e.flag & Element::F_STATIC_SLIP) //continue;
        {
          e.W = {};
          e.V = {};
          continue;
        }

      // first we create a vector of Super-Torque
      vec3d sT ( -e.F[1] / ( siku.planet.R * e.m ),
                       e.F[0] / ( siku.planet.R * e.m ), e.N / e.I );

      // and increment the angular velocity using it (if not steady)
      /*if ( e.flag & Element::F_ANAVEL )
        {
           if (time_diff> 15*60)
           {
             e.flag -= Element::F_ANAVEL;  // turn off forced velocity
             // CSD temporarily reverse the sign of the forcing
             // e.V[1]=-e.V[1];
           }
        } */
      if (( ! ( e.flag & Element::F_STEADY )) && (! (e.flag & Element::F_ANAVEL)))
          e.W += sT * dt;
      // calculating local speed
      //CSD add a viscous damping term to the controll elements.
      if (e.flag & Element::F_CONTROLLED )
      {
       /*   dv = vec3d( e.W.y * siku.planet.R ,
                           -e.W.x * siku.planet.R, 0. ) -e.V;
          e.V = vec3d( e.W.y * siku.planet.R ,
                       -e.W.x * siku.planet.R, 0. );  */
          e.W -= vec3d( e.W.x * e.Vnudge.y , e.W.y * e.Vnudge.x, 0. );
          long check=0; check=check+1;
      }
      e.V = vec3d( e.W.y * siku.planet.R , -e.W.x * siku.planet.R, 0. );
    }
}
