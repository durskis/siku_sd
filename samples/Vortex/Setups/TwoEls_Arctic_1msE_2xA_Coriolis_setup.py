'''Siku scenario

   Sea ice element simple free drift example

   Creates a (large) polygon in polar region and sets some basic winds
   in still water. No interaction with any boundaries, just a free
   float of the polygon in the area and output in KML format of its
   locations.

   Be sure that siku module is in your PYTHONPATH.

   Use python3 for checking. It is not compatible with python2.x

   (c)2014 UAF, written by Anton Kulchitsky
   GPLv3 or later license (same as siku)

'''

import subprocess
import os
import math
import sys
import datetime
import importlib.util
import subprocess
import os
import math
import sys
import datetime
import importlib.util
spec = importlib.util.spec_from_file_location("mathutils","/usr/local/lib/python3.7/site-packages/mathutils-2.79.2-py3.7-linux-x86_64.egg/mathutils.cpython-37m-x86_64-linux-gnu.so")
mathutils=importlib.util.module_from_spec(spec)
spec.loader.exec_module(mathutils)
#import mathutils
import numpy

sys.path.append('/home/sdurski/Siku/Bit/Remod/src')
sys.path.append('/home/sdurski/Siku/Bit/Remod/samples/benchmarks')
import siku
from   siku import polygon
from   siku import element
from   siku import material
from   siku import geocoords
from   siku import regrid
from   siku import gmt_Plotter
GMT_Plotter = gmt_Plotter.GMT_Plotter
from   siku import poly_voronoi
PolyVor = poly_voronoi.PolyVor
from   siku import h5load
hload = h5load.Loader

from shutil import copyfile

from   siku import wnd
 
def main():

    # ---------------------------------------------------------------------
    # Define material
    # ---------------------------------------------------------------------

    ice = material.Material()       # default ice values, 10 thicknesses
    ice.name = 'ice'                # prefer to use our own name instead
                                    # of default
    
    siku.materials.append( ice )    # list of all materials

    # table of material names for convenience
    matnames = {
        'ice': 0,
    }

    # ---------------------------------------------------------------------
    #  Wind initializations (NMC grid example)
    # ---------------------------------------------------------------------

    #Reading in ECMWF files the following seems to work

#    siku.uw = wnd.ECMVar(filename='/home/sdurski/Siku/DataSets/Forcing/Winds/ECMWF_Interim_UVa_Feb_2000_Beaufort_subset.nc', var_name='u10', var_t=0)
#    siku.vw = wnd.ECMVar(filename='/home/sdurski/Siku/DataSets/Forcing/Winds/ECMWF_Interim_UVa_Feb_2000_Beaufort_subset.nc', var_name='v10', var_t=0)
    siku.uw = wnd.ECMVar(filename='/home/sdurski/Siku/DataSets/Forcing/Winds/Zero_winds.nc', var_name='u10', var_t=0)
    siku.vw = wnd.ECMVar(filename='/home/sdurski/Siku/DataSets/Forcing/Winds/Zero_winds.nc', var_name='v10', var_t=0)


#siku.uw = wnd.NMCVar( 'u1994.nc', 'uwnd' )
    #siku.vw = wnd.NMCVar( 'v1994.nc', 'vwnd' )

    # For our initial experiment let's change this to January 1694. Obviously NMCvar doesn't get the timestamp or the variable names right
    #start =  datetime.datetime  ( 1994, 2, 16, 00, 00, 00 )
    start =  datetime.datetime  ( 2000, 2, 9, 00, 00, 00 )
    for i in range(len( siku.uw.times )):
        if siku.uw.times[i] >= start:
            break
# It seems that the idea here is to send the index of the first time after the start time to the wnd.SurfaceVField
# but in that routine it looks like there is a weak attempt at interpolating between two times, in which case I would
# think this should be passing i-1, but the actual current time and time of each wind record needs to get in there too.
# which it doesn't.  For now let's try passing i-1.  This would seem to use the wind at the first time before the start of the simulation.
# As written originally, this crashes in wnd.load_vec if there are only two forcing record times.
#   st_t_ind = i
    st_t_ind = i-1
    siku.time.update_index = i - 1
    print( 'start time: ' + str( start ) + ' at position: ' + str( i ) + \
           ' of ' + str( len( siku.uw.times ) ) + '\n\n' )
    
    siku.wind = wnd.SurfaceVField( siku.uw, siku.vw, st_t_ind )
# I'm not sure what wind_source_type is suppose to be here and
# and the model balks at the WIND_SOURCES field being undefined, so comment it out and leave as default value
# But then siku doesn't think it's a vector field that should be updated...
# it balks at what was in here originally (siku.WIND_SOURCES['NMC'], and it considers the wind_source_type as 1 = TEST
# and doesn't update the field if we leave it undefined, so let's just try a number that we guess might be the
# NMC type - vec field option
#    siku.settings.wind_source_type = siku.WIND_SOURCES['NMC']
    siku.settings.wind_source_type = 2
    siku.settings.wind_source_names = [ '/home/sdurski/Siku/DataSets/Forcing/Winds/Zero_winds.nc' ]
##    w = wnd.NMCSurfaceVField( siku.uw, siku.vw, st_t_ind )
##    w.make_test_field( 0.,0. )
##    siku.wind = w
   
    # ---------------------------------------------------------------------
    # date/time settings
    # ---------------------------------------------------------------------

    #siku.time.start    = datetime.datetime  ( 2012, 3, 12, 00, 00, 00 )
    #siku.time.finish   = datetime.datetime  ( 2012, 3, 13 )
    #siku.time.finish   = datetime.datetime  ( 2012, 3, 12, 00, 00, 10 )
    #siku.time.dt       = datetime.timedelta ( seconds = 1 )
    siku.time.dts      = datetime.timedelta ( seconds = 2700 )
    #siku.time.last = siku.time.start
    hour = datetime.timedelta ( minutes = 60 )

    ## time inits by NMC grid times
    siku.time.start = siku.uw.times[st_t_ind]
    siku.time.last = siku.uw.times[st_t_ind]
    siku.time.last_update = siku.time.last
    siku.time.finish = siku.uw.times[st_t_ind] + hour * 90 #60
    siku.time.dt = datetime.timedelta ( milliseconds = 9000 )
    #siku.time.dt = ( siku.time.finish - siku.time.start ) / 2400 #2400
   
    # ---------------------------------------------------------------------
    # elements
    # ---------------------------------------------------------------------
    
    coords = []
    siku.elements = []

    # ---------------------- voronoi initialization ------------------------
    print('\nLoading polygons')
    
##    PV = PolyVor( '/home/sdurski/Siku/siku/samples/benchmarks/smA.voronoi.xyz',
##                  '/home/sdurski/Siku/siku/samples/benchmarks/smA.voronoi.xyzf' )
##    PV = PolyVor( 'AnC.voronoi.xyz', 'AnC.voronoi.xyzf' )
##    PV = PolyVor( 'AnC1.voronoi.xyz', 'AnC1.voronoi.xyzf' )
##    PV = PolyVor( 'AnC2.voronoi.xyz', 'AnC2.voronoi.xyzf' )
#    PV = PolyVor( '/home/sdurski/Siku/siku/Run/Beaufort_hr/AnC1.voronoi.xyz',
#                  '/home/sdurski/Siku/siku/Run/Beaufort_hr/AnC1.voronoi.xyzf' )
#    PV = PolyVor( '/home/sdurski/Siku/siku/Run/Few_Element_tests/Initial/RectSheetEls2.xyz',
#                  '/home/sdurski/Siku/siku/Run/Few_Element_tests/Initial/RectSheetEls2.xyzf' )
#    PV = PolyVor( '/home/sdurski/Siku/siku/Run/Few_Element_tests/Initial/BeamEls.xyz',
#                  '/home/sdurski/Siku/siku/Run/Few_Element_tests/Initial/BeamEls.xyzf' )
    PV = PolyVor( '/home/sdurski/Siku/Bit/Remod/samples/Vortex/Initial/TwoEls_2xA_Arctic.xyz',
                  '/home/sdurski/Siku/Bit/Remod/samples/Vortex/Initial/TwoEls_2xA_Arctic.xyzf' )
    PV.filter_( 0, 360, 60, 90 )
##    PV.filter_( 190, 230, 62, 82 )
    
    print('Deleting land polygons')
## CSD  Define a bounding box rather than use the gmt coastlines.
    Bndyfile_ocean='/home/sdurski/Siku/Bit/Remod/samples/Vortex/Boundary/Ocean_Arctic_1deg_box_border_1.ll'
    Bndyfile_land='/home/sdurski/Siku/Bit/Remod/samples/Vortex/Boundary/Land_Arctic_1deg_box_border_1.ll'
    rect_ocean_boundary= 'gmt gmtselect temp.lli -F{0} > oceanf.lli'.format(Bndyfile_ocean)
    rect_land_boundary= 'gmt gmtselect temp.lli -F{0} > landf.lli'.format(Bndyfile_land)

    PV.clear_the_land(ocean_filter=rect_ocean_boundary)
#    PV.clear_the_land()

    coords = PV.coords

    siku.tempc = coords # for debug

    ### Initializing elements with polygon vertices
    for c in coords:
        siku.P.update( c )
     
        # Element declaration
        E = element.Element( polygon = siku.P, imat = matnames['ice'] )
        E.monitor = "drift_monitor"
        gh = [ 0.0, 0.0, 1.0, 0.0, 0.0,
               0.0, 0.0, 0.0, 0.0, 0.0 ]
        E.set_gh( gh, ice )
        
        # all elements in the list
        siku.elements.append( E )


    print('Marking borders with GMT')
    bor = PV.get_border_by_gmt(ocean_filter=rect_ocean_boundary,land_filter=rect_land_boundary)
#    bor = PV.get_border_by_gmt()
    for b in bor:
        siku.elements[ b ].flag_state = element.Element.f_static
    print('Done\n\n')

    # ------------------------- speed settings ----------------------------

        #### Start velocities and rotations
    siku.elements[0].velo = (0.1, 0, 0)
    siku.elements[1].velo = (0,0.05, 0)


    # ---------------------S   et the second particle as fixed in place .....
#    siku.elements[0].flag_state = element.Element.f_static
#    siku.elements[1].flag_state = element.Element.f_static
    # ---------------------- loading from file ----------------------------

##    print('loading from file\n')
##    
##    hl = hload('save_test.h5')
####    #hl = hload('siku-2014-01-01-12:50:46.h5')
####
####    #hl.load()
##    hl.load_fnames()
##    hl.load_mats()
##    hl.load_els()
##    print('\n')
##
##    siku.elements = hl.extract_els()
##    siku.materials = hl.extract_mats()
##          
##    hl = None

    # ---------------------------------------------------------------------
    #  Monitor function for the polygon
    # ---------------------------------------------------------------------

    ## Plotter initialization
    siku.plotter = GMT_Plotter( '/home/sdurski/Siku/siku/samples/benchmarks/beaufortECM_plot.py' )

    ### period of picturing
    siku.diagnostics.monitor_period = 300
    siku.drift_monitor = drift_monitor
    siku.diagnostics.step_count = 0

    siku.settings.contact_method = siku.CONTACT_METHODS['sweep']
    siku.settings.force_model = \
                    siku.CONTACT_FORCE_MODEL['distributed_spring']

    # name of file to load from
    #siku.settings.loadfile = 'siku-2014-01-01-12:00:00.h5'
##    siku.settings.loadfile = 'save_test.h5'

## Old settings ...
#    siku.settings.phys_consts = { 'rigidity' : 1.1,
#                                  'viscosity' : 1.0,
#                                  'rotatability' : 0.75,  #0.75
#                                  'tangency' : -0.00003,  #-0.00003
#
#                                  'elasticity' : 5000.0,  #-5000000.0,
#                                  'bendability' : 1.0,  #1.0,
#                                  'solidity' : 0.5,#0.05,
#                                  'tensility' : 1.3,#0.615,
#
#                                  'anchority' : 0.0005,
#                                  'windage':    0.0005,
#                                  'fastency' : 0.30, #0.5
#
#                                  'sigma' : 100000.0,        # -//- rigidity
#                                  'etha' : 0.0051,          # -//- viscosity
#                                  'dest_threshold' : 0.9
#                                  }

    siku.settings.phys_consts = {'rigidity': 100.0,
                                 'viscosity': 1.0,
                                 'rotatability': 0.75,  # 0.75
                                 'tangency': -0.00003,  # -0.00003

                                 'elasticity': 5000.0,  # -5000000.0,
                                 'bendability': 1.0,  # 1.0,
                                 'solidity': 0.5,  # 0.05,
                                 'tensility': 0.615,  # 0.615,

                                 'anchority': 0.0,  # 0.0005
                                 'windage': 0.0,  # 0.0005
                                 'wind_angle': 0,  # wind turning angle (degrees, positive clockwise)
                                 'water_angle': 0, # ocean current turning angle (degrees)
                                 'fastency': 0.30,  # 0.5

                                 'sigma': 250000.0,  # -//- rigidity
                                 'etha': 10,  # -//- viscosity  0.0051
                                 'dest_threshold': 0.9,

                                 'periodRot': 86400 # earth period of rotation in seconds
                                 }

##  =====================  Specify the output file prefix  ==========================================

    siku.output_file_prefix: str='TwoEls_Arctic_1msE_2xA_Coriolis'


    #  Let's save THIS file along with the material file to the Output directory with a filename that links it with
    #  the h5 file being generated for this particular simulation
    THIS_file: str = os.path.join(os.getcwd(),'Two_Elements_Arctic.py')
    MATRL_file: str = '/home/sdurski/Siku/Bit/Remod/python/siku/material.py'
    Setup_file: str = os.path.join(os.getcwd()+'/Setups', siku.output_file_prefix + '_setup.py')
    Material_file: str = os.path.join(os.getcwd()+'/Setups', siku.output_file_prefix + '_material.py')

    copyfile(THIS_file, Setup_file)
    copyfile(MATRL_file, Material_file)

    ##    siku.settings.contact_freq_met = siku.CONTACT_DET_FREQ_MET['speed']
##    siku.settings.contact_value = 1000

    # ---------------------------------------------------------------------
    #  Diagnostics function for the winds
    # ---------------------------------------------------------------------
    
##    # We create a grid and append it to monitor grids
##    siku.diagnostics.wind_counter = 0
##    rg = regrid.Regrid()
##    mesh_01 = rg.globe_coverage( 5.0 )
##    siku.diagnostics.meshes.append( mesh_01 )
##    siku.diagnostics.wind.append( 
##        ( winds_di
    #
    #
    #
    # ag, 0, siku.time.start, 2*siku.time.dt ) )

    # ---------------------------------------------------------------------
    #  Settings
    # ---------------------------------------------------------------------

    # ---------------------------------------------------------------------
    #  Callback flag-mask generator
    # ---------------------------------------------------------------------

    siku.callback.pretimestep = pretimestep
    siku.callback.aftertimestep = aftertimestep
    siku.callback.conclusions = conclusions
    siku.callback.initializations = initializations
    siku.callback.updatewind = updatewind

    ##
    siku.callback.presave = presave

    siku.err_test = {}
    
    return 0

def presave( t, n, ns ):

    return os.path.join('Output',siku.output_file_prefix+'_%03d.h5' % ns)

# --------------------------------------------------------------------------

def initializations( siku, t ):
    subprocess.call(["gmtset", "PS_MEDIA=Custom_17cx13c"])

# --------------------------------------------------------------------------

def conclusions( siku, t ):
    
##    with open("err_time.txt", 'w') as erf:
##        for i in siku.err_test:
##            erf.write( str(i) + ' : ' )#+ ':\n' )
##            erf.write( str( len( siku.err_test[i] ) ) )
####            for t in siku.err_test[i]:
####                erf.write( str( t ) + '   ' )
##            erf.write( '\n' )

# CSD don't bother converting eps to gifs right now
#    print('creating .gif')
#    subprocess.call( "nice convert -density 300 -delay 10 beaufort*.eps beaufort.gif", \
#                     shell=True )
    pass

# --------------------------------------------------------------------------

def pretimestep( t, n, ns ):
    status = siku.MASK['NONE']
    siku.diagnostics.step_count = n

    siku.local.poly_f = open( 'Polygons.txt', 'w' )
    if siku.diagnostics.step_count % siku.diagnostics.monitor_period == 0:
#        Poly_file_name = 'Vortex_%03d.asc' % \
#                     (siku.diagnostics.step_count / siku.diagnostics.monitor_period)
#        siku.local.poly_f2 = open(os.path.join('Output',Poly_file_name), 'w')
#update the winds if either there is a new time record available or if it's a
# diagnostic monitoring step.  This is a compromise between updating on each
# timestep and abruptly on each new day.  updatewind should linearly interpolate
# between time records, for this to have any effect.  CSD
        status += siku.MASK['WINDS']
    # some specific checks should be placed.

    # primitive time stepping
##    if t > ( siku.time.last + siku.time.dt ):
##        status += siku.MASK['WINDS']
##        siku.time.last = t

    # step by NMC own time step
    if t >= siku.uw.times[siku.time.update_index + 1]: # siku.time.last: #
        status = max([status, siku.MASK['WINDS']])
        siku.time.last = t# siku.time.finish#

    # and change the winds here
    # ~!wind is changed with another call

    # and save the current time in a structure
    # ~!current time is saved in siku.time.last
    return status

# --------------------------------------------------------------------------

def updatewind( siku, t ):
# Instead of updating the wind only when there's a new time record
# available, lets (for now) update it also every diagnostic monitoring step
# with a linear interpolation between wind records.  So instead of update_index
# now we want a real number, the fractional part indicating how far
# between wind time records we are at this timestep.  But we preserve update index
# as it was originally acting, in case it's used somewhere else.
#    siku.time.update_index += 1
    trec_findex = (t.timestamp() - siku.uw.times[0].timestamp())/ \
                    (siku.uw.times[1].timestamp()-siku.uw.times[0].timestamp())
    siku.time.update_index = max(siku.time.update_index,int(trec_findex))
    siku.time.last_update = t
#    siku.wind = \
#              wnd.NMCSurfaceVField( siku.uw, siku.vw, siku.time.update_index )
# Since we have been working with ECM winds we just define  a SurfaceVField Class.
    siku.wind = \
              wnd.SurfaceVField( siku.uw, siku.vw, trec_findex)
    print( 'Done interpolating winds at '+ str( t ) + '\n' )
    pass

# --------------------------------------------------------------------------

def aftertimestep( t, n, ns ):
    siku.local.poly_f.close()
#    siku.local.poly_f2.write('hey \n')
#    siku.local.poly_f2.close()
    if siku.diagnostics.step_count % siku.diagnostics.monitor_period == 0:
        recnum=siku.diagnostics.step_count / siku.diagnostics.monitor_period
#        pic_name = 'beaufort%03d.eps' % recnum
#        print('drawing ' + str( pic_name ) )

#        siku.plotter.plot( pic_name, siku.time.update_index, siku.wind, recnum )

    #siku.local.poly_f.close()
    return 0

# --------------------------------------------------------------------------

def drift_monitor( t,n, Q, Ps, st):
##   CSD ...leaving these varibales off for now  ..., index, ID, W, F, N, S, m, I, i, A, a_f, w_f ):
##    #static polygons (generally shores) may be simply passed
##    if st & element.Element.f_static:
##        return

    # create actual quaternion
    q = mathutils.Quaternion( Q )
    C = mathutils.Vector( (0,0,1) )

    # get latitude and longitude of center of mass (0,0,1)
    R = q.to_matrix()
    c = R * C
    # appending vertices to plotting list
    if siku.diagnostics.step_count % siku.diagnostics.monitor_period == 0:        
        Pglob = [ R*mathutils.Vector( p ) for p in Ps ]
        vert = [ geocoords.lonlat_deg(mathutils.Vector( p ) ) for p in Pglob ]

        poly = siku.local.poly_f

##        if st & element.Element.f_errored: ##
##            poly.write( '> -Gred -W0.1p,red \n' ) ##
        if st & element.Element.f_special: ## elif -> if
##        elif st & element.Element.f_special: ## elif -> if
            poly.write( '> -Gpurple -W0.1p,pink \n' )
#            siku.local.poly_f2.write(str(len(vert))+'\t'+'1 \n')
        elif st & element.Element.f_static:
##            return
            poly.write( '> -Gbrown -W0.1p,lightBlue \n' )#<<--- this
#            siku.local.poly_f2.write(str(len(vert))+'\t'+'2 \n')
##            poly.write( '> -GlightCyan -W0.1p,lightBlue \n' )
        elif st & element.Element.f_steady:
            poly.write( '> -GlightGreen -W0.1p,lightBlue \n' )
#            siku.local.poly_f2.write(str(len(vert))+'\t'+'3 \n')
        else:
            poly.write( '> -GlightCyan -W0.1p,lightBlue \n' )
#            siku.local.poly_f2.write(str(len(vert))+'\t'+'4 \n')
##            poly.write( '> -GlightCyan -W0.1p,lightBlue \n' )
#        for v in vert:
#            siku.local.poly_f2.write( str( geocoords.norm_lon(v[0]) )+'\t'+ \
#                        str( v[1] )+'\n' )

        for v in vert:
            poly.write( str( geocoords.norm_lon(v[0]) )+'\t'+ \
                        str( v[1] )+'\n' )

    return

# --------------------------------------------------------------------------

def winds_diag( t, winds ):

    mesh = siku.diagnostics.meshes[0]
    ez = mathutils.Vector( (0,0,1) )

###### Commented to stop that file breeding while other modules are being tested
##    fp = open( 'winds-%02d.txt' % (siku.diagnostics.wind_counter), 'w' )
##
##    for i, w in enumerate( winds ):
##        x = mathutils.Vector( mesh[i] )
##        u = mathutils.Vector( w )
##        uval = u.length
##        lon, lat = geocoords.lonlat_deg( x )
##        a = ez - x
##        
##        mdl = a.length * uval
##        if ( mdl != 0 ):
##            azimuth = 180 * math.acos( (a*u) / mdl ) / math.pi
##            fp.write( "%f %f %f %f %f\n" % \
##                      ( lon, lat, 0.25*uval, azimuth, 0.7*uval ) )
##            
##
##    fp.close()
    siku.diagnostics.wind_counter += 1

    return

# ---------------------------------------------------------------------
# Calling main function at the end
# ---------------------------------------------------------------------

siku.main = main()

if __name__ == '__main__':
    sys.exit( siku.main )
