'''Siku: Sea Ice Discrete Element Method Model
   File: material.py
   Basic template class for material data

'''

class Material:
    '''Default material.

    Data for thickness (10 layers) are used based on Jia Wang
    et.al. report "Sea Ice-Ocean-Oilspill Modeling System (SIOMS)
    for the Nearshore Beaufort and Chukchi Seas: Parameterization
    and Improvement (Phase II). Final Report, OCS Study MMS
    2008-021. February 2010.

    sigma_t from Hopkins 2003. sigma_c also.

    '''
    def __init__(self):
        self.name = 'Default ice'
        self.thickness_intervals = [1.0, 2.0]  # m NOTE: len() must be less than SIKU_MAX_THICK
        self.rho = [910.0] * len(self.thickness_intervals)  # kg/m^3
        self.sigma_t = [6.425] * len(self.thickness_intervals)  # kPa
        self.sigma_c = [64.25] * len(self.thickness_intervals)  # kPa
        self.E = 1.0e9  # Pa
        ''' self.sigma_t = [6.425] * len(self.thickness_intervals)  # kPa
        self.sigma_c = [64.25] * len(self.thickness_intervals)  # kPa '''
        self.nu = 0.3  # 0.3  # --for shear modulus
        self.mu = 0.6  # 0.6  # -- friction parameter

    @classmethod
    def weak_single_category_ice(cls):
        mprop = cls.__new__(cls)
        mprop.name = 'broken ice'
        mprop.thickness_intervals = [ 1.0, 2.0] # m
        mprop.rho = [910.0]*len( mprop.thickness_intervals )     # kg/m^3
        mprop.sigma_t = [ 1.285] *len( mprop.thickness_intervals )   # kPa
        mprop.sigma_c = [ 12.85]*len( mprop.thickness_intervals ) # kPa
        mprop.E  = 1e8       # Pa
        mprop.nu = 0.3       # 0.3
        mprop.mu = 0.6       # 0.6
        return mprop

    @classmethod
    def ordinary_coast(cls):
        mprop = cls.__new__(cls)
        mprop.name = 'coastal material'
        mprop.thickness_intervals = [1.0, 2.0]  # m
        mprop.rho = [910.0] * len(mprop.thickness_intervals)  # kg/m^3
        mprop.sigma_t = [1.285] * len(mprop.thickness_intervals)  # kPa
        mprop.sigma_c = [12.85] * len(mprop.thickness_intervals)  # kPa
        mprop.E = 1e9  # Pa
        mprop.nu = 0.3  # 0.3
        mprop.mu = 0.6  # 0.6
        return mprop
