'''
     Siku: Earth default parameters

     These parameters are default planet settings for siku

'''

class Earth:
    '''
    List of main spherical Earth parameters
    '''
    ''' for consistency with the c++ code the earth radius should be 6371000.  
        This is the average earth radius. '''
    ''' radius = 6353000.0          # m  '''
    radius = 6371000.0
    pass

